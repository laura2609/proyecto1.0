﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Animator))]
public class Actions : MonoBehaviour {

	private Animator animator;
    public bool flex=false;
    public bool ABDUP = false;
    public bool idle = false;


    //	const int countOfDamageAnimations = 3;
    //int lastDamageAnimation = -1;



    void Awake () {
		animator = GetComponent<Animator> ();
	}
    public void Update()
    {
        if (flex == true)
        {
            //animator.SetBool("PosInicial", true);
           animator.SetBool("Flex", false);
            animator.SetBool("ABDUP", false);
            StartCoroutine(FlexWait());
            
        }
       
        if (ABDUP == true)
        {
            animator.SetBool("ABDUP", false);
            animator.SetBool("Flex", false);
            StartCoroutine(ABDUPWait());
        }
       
    }

    public void Flex()
    {
        //flex = !flex;
        flex = true;
        ABDUP = false;
    }
    public void ABDUPP()
    {
        ABDUP = true;
        flex = false;
    }
    public void idles()
    {
        idle = true;
        flex = false;
        ABDUP = false;
        animator.SetBool("Idle", true);
        animator.SetBool("Flex", false);
        animator.SetBool("ABDUP", false);
    }


    IEnumerator FlexWait()
    {
        yield return new WaitForSeconds(3);
        //animator.SetBool("PosInicial", false);
        animator.SetBool("Flex", true);
    }
    IEnumerator ABDUPWait()
    {
        yield return new WaitForSeconds(3);
        //animator.SetBool("PosInicial", false);
        animator.SetBool("ABDUP", true);
    }

    /*public void Stay () {
        animator.SetBool("Aiming", false);
        animator.SetFloat ("Speed", 0f);
        }

    public void Walk () {
        animator.SetBool("Aiming", false);
        animator.SetFloat ("Speed", 0.5f);
    }

    public void Run () {
        animator.SetBool("Aiming", false);
        animator.SetFloat ("Speed", 1f);
    }

    public void Attack () {
        Aiming ();
        animator.SetTrigger ("Attack");
    }

    public void Death () {
        if (animator.GetCurrentAnimatorStateInfo (0).IsName ("Death"))
            animator.Play("Idle", 0);
        else
            animator.SetTrigger ("Death");
    }

    public void Damage () {
        if (animator.GetCurrentAnimatorStateInfo (0).IsName ("Death")) return;
        int id = Random.Range(0, countOfDamageAnimations);
        if (countOfDamageAnimations > 1)
            while (id == lastDamageAnimation)
                id = Random.Range(0, countOfDamageAnimations);
        lastDamageAnimation = id;
        animator.SetInteger ("DamageID", id);
        animator.SetTrigger ("Damage");
    }

    public void Jump () {
        animator.SetBool ("Squat", false);
        animator.SetFloat ("Speed", 0f);
        animator.SetBool("Aiming", false);
        animator.SetTrigger ("Jump");
    }

    public void Aiming () {
        animator.SetBool ("Squat", false);
        animator.SetFloat ("Speed", 0f);
        animator.SetBool("Aiming", true);
    }

    public void Sitting () {
        animator.SetBool ("Squat", !animator.GetBool("Squat"));
        animator.SetBool("Aiming", false);
    }
    */
}
