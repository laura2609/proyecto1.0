﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SalirScena : MonoBehaviour {
    //En escena Pacients, Scrollview/canvas/CerrarSesion
    //Permite, al pulsar el boton, cerrar sesion y volver a la escea inicial
    public void ButtonClick()
    {
        SceneManager.LoadScene("GlobalAccountSystem", LoadSceneMode.Single);
    }
}
