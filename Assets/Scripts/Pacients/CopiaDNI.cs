﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// En la escena Pacientes, en el Panel. Busca el GameObject con el tag "DNI"del doctor, que corresponde al DNI que se coge con DontDestroyOnLoad. Permite que aparezca el DNI del doctor en la escena Pacientes arriba a al izq. 
/// </summary>
public class CopiaDNI : MonoBehaviour {
    public Text mytext=null;
    public GameObject[] DNI;
    public string nombre;

	// Update is called once per frame
	void Update () {
        DNI = GameObject.FindGameObjectsWithTag("DNI");
        nombre = DNI[0].name;
        mytext.text = nombre;

      

    }
}
