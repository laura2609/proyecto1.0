﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic; //
using System;
using System.Text;
using UnityEngine.EventSystems;
using System.Text.RegularExpressions;
using System.Linq;

/// <summary>
/// Se encuentra en la escena Pacients-> en el Content. Pide al servidor con el SeleccionDoctor.php que le de todos los pacientes de un Doctor (DNI doctor) especifico,
/// despues hace un split de la lista de pacientes
/// 
/// </summary>

public class Listado : MonoBehaviour
{

    public GameObject DDNI;
    private Text DNI;
    public Text mytext = null;
    public String[] Pacientes;

    string LoginURL = "localhost/users/SeleccionDoctor.php";

    // Use this for initialization
    void Start()
    {
        DNI = DDNI.GetComponent<Text>();//DNi del doctor
       

    }

    void Update()
    {
        StartCoroutine(Lista1(DNI.text));

    }

    IEnumerator Lista1(string DNI)
    {

        WWWForm form = new WWWForm();


        form.AddField("DNIPost", DNI); //Envia el DNI del Dr al servidor


        WWW www = new WWW(LoginURL, form);

        yield return www;

       

        var Clientes = www.text.Split(new string[] { "\n" }, StringSplitOptions.None); //Separa el mensaje en cada salto de linea 

        Pacientes = Clientes;
        Debug.Log(Pacientes[0]);


    }


}
