﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


//Dentro Main camera, Escena Pacients
//FUncion: El back button del register invoca a este script que invoca al boton refresh.
//Ek boton refresh llama al ShopScroll list que esta dentro del panel "content "
//Esto permite que al darle al boton back haga un refresh automaticamente de los pacientes 
public class RefreshRemoto : MonoBehaviour
{
    public Button myButton;//boton Refresh

    // Use this for initialization
    void Start()
    {

    }
    public void ButtonClick()
    {
        StartCoroutine(Temporizador());


}

    IEnumerator Temporizador() //Dado que no puede Eliminar y Refrescar pacientes a la vez se le pone un pequeño temporizador de 0.2 Seg
    {
        yield return new WaitForSeconds(0.2f);
        myButton.onClick.Invoke();
    }
}
