﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    public Text Contador_1;
    public float Tiempo = 10f;
    private bool started = false;
	// Use this for initialization
	void Start () {
        Contador_1.text = "" + Tiempo; 
	}
	
	// Update is called once per frame
	void Update () {
        if(started && Tiempo > 0) {
            Tiempo -= Time.deltaTime;
            Contador_1.text = "" + Tiempo.ToString("f0");
        }
        
        //Debug.Log("Timer: update");
	}

    public void ButtonClick()
    {
        StartCoroutine(waitAndCount());
  
    }

    IEnumerator waitAndCount()
    {
        yield return new WaitForSeconds(7);
        started = true;

    }
}
