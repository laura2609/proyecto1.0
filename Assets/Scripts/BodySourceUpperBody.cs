﻿using UnityEngine;
using System.Collections;
using Windows.Data;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using Windows.Kinect;
using System.Linq;
using System.IO;
using System;
using UnityEngine.UI;



public class BodySourceUpperBody : MonoBehaviour
{
    public Material BoneMaterial;
    public GameObject BodySourceManager;

    public GameObject _personaje1_Unity;
    public GameObject Master;
    public GameObject Reference;
    public GameObject Hips;
    public GameObject LeftUpLeg;
    public GameObject LeftLeg;
    public GameObject RightUpLeg;
    public GameObject RightLeg;
    public GameObject Spine;
    public GameObject Spine1;
    public GameObject Spine2;
    public GameObject LeftShoulder;
    public GameObject LeftArm;
    public GameObject LeftForeArm;
    public GameObject LeftHand;
    public GameObject Neck;
    public GameObject Head;
    public GameObject RightShoulder;
    public GameObject RightArm;
    public GameObject RightForeArm;
    public GameObject RightHand;
    public GameObject LeftFoot; //
    public GameObject RightFoot; //
    public GameObject RightAnkle; //
    public GameObject LeftAnkle; //
    public float a = 0;
    public float a2 = 0;
    public float b = 0;
    public float b2 = 0;
    public float maxtot = 0;
    public Text derecha = null;
    public Text izquierda = null;
    public int cont = -1;
    public bool rep = true;
    public Text angulo = null;
    public Text angulo2 = null;
    public bool angulonum = false;
    public GameObject semaforo;
    public GameObject semaforo2;
    public Text copiaej = null;
    public bool Elbow=false;
    public bool ABDUP = false;
    public float valor;
    public float valor2;
    public Text Simetria;
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;
    public Text Simetrianum;
    public Text repeticionesnum;
    public bool repeticiones0=false;
    public Text AngleR;
    public Text AngleL;

    //public Text UserName;
    //public Text DNI; 

    
    



    /// <summary>
    /// PARTE CANVAS MAX VALUE
    /// </summary>
    public bool flex = false;

    
    public void Flex()
    {
        flex = true;
        StartCoroutine(Seemaforo());
    }

    public void Elboww() {
        Elbow = !Elbow;
        StartCoroutine(Seemaforo());
    }

    public void ABDUPP()
    {
        ABDUP = true;
        StartCoroutine(Seemaforo());
    }





    /// <summary>
    /// /////////////////////////////
    /// </summary>

    // Mapeo "puntos" del Cuerpo Humano

    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    public void Start()
    {

        _personaje1_Unity = GameObject.Find("personaje1_Unity");
        Master = GameObject.Find("master");
        Reference = GameObject.Find("Reference");
        Hips = GameObject.Find("Hips");
        LeftUpLeg = GameObject.Find("LeftUpLeg");
        LeftLeg = GameObject.Find("LeftLeg");
        RightUpLeg = GameObject.Find("RightUpLeg");
        RightLeg = GameObject.Find("RightLeg");
        Spine = GameObject.Find("Spine");
        Spine1 = GameObject.Find("Spine1");
        Spine2 = GameObject.Find("Spine2");
        LeftShoulder = GameObject.Find("LeftShoulder");
        LeftArm = GameObject.Find("LeftArm");
        LeftForeArm = GameObject.Find("LeftForeArm");
        LeftHand = GameObject.Find("LeftHand");
        Neck = GameObject.Find("Neck");
        Head = GameObject.Find("Head");
        RightShoulder = GameObject.Find("RightShoulder");
        RightArm = GameObject.Find("RightArm");
        RightForeArm = GameObject.Find("RightForeArm");
        RightHand = GameObject.Find("RightHand");
        RightAnkle = GameObject.Find("RightToeBase"); //
        LeftAnkle = GameObject.Find("LeftToeBase");   //
        RightFoot = GameObject.Find("RightFoot");  //
        LeftFoot = GameObject.Find("LeftFoot");    //



        //////// Crear nova entrada al ftxer .xls

        //DNI = LoginPaciente.Instantiate(UserName);
        
        string path = Application.dataPath + "/Seguimiento/AnlgeR.xls";

        if (File.Exists(path))
        {
            File.AppendAllText(path, "\n Login date: " + System.DateTime.Now + "\n");
        }

        string path2 = Application.dataPath + "/Seguimiento/AnlgeL.xls";

        if (File.Exists(path2))
        {
            File.AppendAllText(path2, "\n Login date: " + System.DateTime.Now + "\n");
        }


    }




    IEnumerator Seemaforo()
    {
        Debug.Log("Hola");
        copiaej.text = "Copie la posicion de la entrenadora!";
        yield return new WaitForSeconds(3);
        Destroy(semaforo);
        copiaej.text = "Ahora es su turno!";
        semaforo2.SetActive(true);
    }

    IEnumerator Temporizador(float angleFlex, float angleFlexl)
    {
        flex = false;
        yield return new WaitForSeconds(0.5f);//Mira angulos FLex Cada segundo
        MAX(angleFlex, angleFlexl);
        flex = true;

    }
    IEnumerator TemporizadorABDUP(float correct, float correctl)
    {

        ABDUP = false;
        yield return new WaitForSeconds(0.5f);//Mira angulos FLex Cada segundo
        MAXABDUP(correct,correctl);
        ABDUP = true;

    }
    public void MAXABDUP(float correct, float correctl)
    {

        if (correct > a)
        {
            a = correct;


        }
        if (correct > a2)
        {
            a2 = correctl;
        }
        valor = correct;
  
        valor2 = correctl;

        
        AngleR.text = ( "" + valor ); // Creamos el vector para los angulos (frame a frame) del brazo derecho
        Debug.Log(AngleR.text);
        AngleL.text = ("" + valor2); // Creamos el vector para los angulos (frame a frame) del brazo izquierdo
        
        ////////////////////////.xls para AngleR
       

        string path = Application.dataPath + "/Seguimiento/AnlgeR.xls"; //Creamos un documento de excel (.xls) para guardar los datos
        string content = AngleR.text + "\n" ;   //Contenido del .xls

        if (!File.Exists(path))
        {
            File.AppendAllText(path, "Data Angulo Derecho \n Login date: " + System.DateTime.Now + "\n");
        }

        File.AppendAllText(path,content);
        
      
        
        ////////////////////////.xls para AngleL


        string path2 = Application.dataPath + "/Seguimiento/AnlgeL.xls"; //Creamos un documento de excel (.xls) para guardar los datos
        string content2 = AngleL.text + "\n";   //Contenido del .xls

        if (!File.Exists(path2))
        {
            File.WriteAllText(path2, "Data Angulo Izquierdo \n Login date: " + System.DateTime.Now + "\n");
        }

        File.AppendAllText(path2, content2);


        Repeticiones(valor);
        derecha.text = ("" + correct); //Actual
        izquierda.text = ("" + correctl);//Actual iz
        angulo.text = (""+ a);//Valor maximo derecho
        angulo2.text = ("" + a2);//Valor maximo iz
        if (((correct-correctl)>b))
        {
            b = correct - correctl;
            Simetria.text = "Esta desviado la derecha en :" + b + " Grados";

        }
        if (((correctl - correct) > b))
        {
            b = correctl - correct;
            Simetria.text = "Esta desviado la izquierda en :" + b + " Grados";
            Simetrianum.text = ("" + b);
            Debug.Log(b);

        }
    

    }
    public void MAX(float angleFlex, float angleFlexl)
    {

        if (angleFlex > a)
        {
            a = angleFlex;

        }
        if (angleFlexl > a2)
        {
            a2 = angleFlexl;

        }
        valor = angleFlex;
        valor2 = angleFlexl;
        Repeticiones(valor);
        derecha.text = ("" + angleFlex);
        izquierda.text = ("" + angleFlexl);
        angulo.text = (""+ a);//si falla al hacer graficas, poner un "0".
        angulo2.text = ("" + a2);
        //PONER LAS REPETIVIONES-> CONT
        if (((angleFlex - angleFlexl) > b))
        {
            b = angleFlex - angleFlexl;
            Simetria.text = "Esta desviado la derecha en :" + b + " Grados";
            Simetrianum.text = ("" + b);

        }
        if (((angleFlexl - angleFlex) > b))
        {
            b = angleFlexl - angleFlex;
            Simetria.text = "Esta desviado la izquierda en :" + b + " Grados";

        }
    }

    public void Repeticiones(float valor)
    {
        repeticionesnum.text = ("" + cont);
        if (valor < 20 && rep == true)
        {
            if (repeticiones0 == true)
            {
                cont = cont + 1;
              
              
            }
            
            
            rep = false; 
            Debug.Log("repeticion numero" + " " + cont); //(cont)
            
            //Debug.Log("Valor maximo enviado :   " + a); // CAMBIOOOOO LAURA
            //numrep=  cont;// guarda el numero de repeticiones


            if (cont == 5) //Mirar poner un 5
            {
            Debug.Log("Valor maximo enviado :   " + a);

            }

        }
        if (valor > 20)
        {
            rep = true;
            repeticiones0 = true;

        }


    }
    void Update()
    {

        if (BodySourceManager == null)
        {
            return;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }

        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }

        var floorPlane = _BodyManager.FloorPlanes;
        var newrotation = Quaternion.FromToRotation(new Vector3(floorPlane.X, floorPlane.Y, floorPlane.Z), Vector3.up);


        List<ulong> trackedIds = new List<ulong>();
        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                trackedIds.Add(body.TrackingId);
            }
        }

        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);

        // First delete untracked bodies
        foreach (ulong trackingId in knownIds)
        {
            if (!trackedIds.Contains(trackingId))
            {

                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }

        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                if (!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }

                RefreshBodyObject(body, _Bodies[body.TrackingId]);

                Quaternion SpineBase = body.JointOrientations[JointType.SpineBase].Orientation.ChangeQuat(newrotation);
                Quaternion SpineMid = body.JointOrientations[JointType.SpineMid].Orientation.ChangeQuat(newrotation);
                Quaternion SpineShoulder = body.JointOrientations[JointType.SpineShoulder].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderLeft = body.JointOrientations[JointType.ShoulderLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderRight = body.JointOrientations[JointType.ShoulderRight].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowLeft = body.JointOrientations[JointType.ElbowLeft].Orientation.ChangeQuat(newrotation);
                Quaternion WristLeft = body.JointOrientations[JointType.WristLeft].Orientation.ChangeQuat(newrotation);
                Quaternion HandLeft = body.JointOrientations[JointType.HandLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowRight = body.JointOrientations[JointType.ElbowRight].Orientation.ChangeQuat(newrotation);
                Quaternion WristRight = body.JointOrientations[JointType.WristRight].Orientation.ChangeQuat(newrotation);
                Quaternion HandRight = body.JointOrientations[JointType.HandRight].Orientation.ChangeQuat(newrotation);
                Quaternion KneeLeft = body.JointOrientations[JointType.KneeLeft].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleLeft = body.JointOrientations[JointType.AnkleLeft].Orientation.ChangeQuat(newrotation);
                Quaternion KneeRight = body.JointOrientations[JointType.KneeRight].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleRight = body.JointOrientations[JointType.AnkleRight].Orientation.ChangeQuat(newrotation);
                //Quaternion LegRight = body.JointOrientations[JointType.RightLeg].Orientation.ChangeQuat(newrotation); //
                //Quaternion LegLeft = body.JointOrientations[JointType.LeftLeg].Orientation.ChangeQuat(newrotation); //
                Quaternion FootRight = body.JointOrientations[JointType.FootRight].Orientation.ChangeQuat(newrotation); //
                Quaternion FootLeft = body.JointOrientations[JointType.FootLeft].Orientation.ChangeQuat(newrotation); //

                FootRight.y = 1;
                FootRight.w = 0;
                FootLeft.y = 1;
                FootLeft.w = 0;
                Spine1.transform.rotation = SpineMid * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightArm.transform.rotation = ElbowRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightForeArm.transform.rotation = WristRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightHand.transform.rotation = HandRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftArm.transform.rotation = ElbowLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftForeArm.transform.rotation = WristLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightFoot.transform.rotation = FootRight * Quaternion.AngleAxis(0, new Vector3(0, 0, 1)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0)); // ojo -90

                LeftFoot.transform.rotation = FootLeft * Quaternion.AngleAxis(180, new Vector3(1, 0, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));//
                LeftHand.transform.rotation = HandLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightUpLeg.transform.rotation = KneeRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightLeg.transform.rotation = AnkleRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftUpLeg.transform.rotation = KneeLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftLeg.transform.rotation = AnkleLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

                var moveposition = body.Joints[JointType.SpineMid].Position;
                Master.transform.position = new Vector3(moveposition.X, moveposition.Y, -moveposition.Z);



                /////////////////superior

                // ::::::::::::::::::::   HORIZONTAL ARMS EXERCISE   :::::::::::::::::::: // 

                if (Input.GetKeyDown(KeyCode.Q))
                {
                    Debug.Log("Horizontal Exercise selected");
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", " ::::::   HORIZONTAL EXERCISE   :::::: ");
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);

                }

                // ::::::::::::::::::::   TOUCH SHOULDERS EXERCISE   :::::::::::::::::::: // 

                if (Input.GetKeyDown(KeyCode.W))
                {
                    Debug.Log("Touch Shoulders Exercise selected");
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", " ::::::   TOUCH SHOULDERS EXERCISE   :::::: ");
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);

                }

                // ::::::::::::::::::::   ARMS UP EXERCISE   :::::::::::::::::::: // 

                if (Input.GetKeyDown(KeyCode.E))
                {
                    Debug.Log("Arms Up Exercise Selected");
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", " ::::::   ARMS UP EXERCISE   :::::: ");
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);
                }

                // ::::::::::::::::::::   FLEX   :::::::::::::::::::: //
                if (flex == true)
                {

                    //variables
                    float x1 = body.Joints[JointType.ShoulderRight].Position.X;
                    float y1 = body.Joints[JointType.ShoulderRight].Position.Y;
                    float z1 = body.Joints[JointType.ShoulderRight].Position.Z;
                    float x2 = body.Joints[JointType.ShoulderLeft].Position.X;
                    float y2 = body.Joints[JointType.ShoulderLeft].Position.Y;
                    float z2 = body.Joints[JointType.ShoulderLeft].Position.Z;
                    float x3 = body.Joints[JointType.SpineMid].Position.X;
                    float y3 = body.Joints[JointType.SpineMid].Position.Y;
                    float z3 = body.Joints[JointType.SpineMid].Position.Z;
                    float u1 = body.Joints[JointType.SpineBase].Position.X;
                    float u2 = body.Joints[JointType.SpineBase].Position.Y;
                    float u3 = body.Joints[JointType.SpineBase].Position.Z;
                    float c1 = body.Joints[JointType.ElbowRight].Position.X;
                    float c2 = body.Joints[JointType.ElbowRight].Position.Y;
                    float c3 = body.Joints[JointType.ElbowRight].Position.Z;
                    float c1l = body.Joints[JointType.ElbowLeft].Position.X;
                    float c2l = body.Joints[JointType.ElbowLeft].Position.Y;
                    float c3l = body.Joints[JointType.ElbowLeft].Position.Z;

                    //planes
                    Vector3 vector1 = new Vector3(x2 - x1, y2 - y1, z2 - z1);
                    Vector3 vector2 = new Vector3(x3 - x1, y3 - y1, z3 - z1);
                    Vector3 cp;
                    cp = Vector3.Cross(vector1, vector2);
                    Vector3 normalvect = new Vector3(cp.x, cp.y, cp.z);
                    Vector3 auxvect2 = new Vector3(x3 - u1, y3 - u2, z3 - u3);
                    Vector3 newcp;
                    newcp = Vector3.Cross(normalvect, auxvect2);
                    float daux = newcp.x * (-x3) + newcp.y * (-y3) + newcp.z * (-z3);

                    //right side projection and result
                    float lambdaaux = (((-newcp.x) * c1 + (-newcp.y) * c2 + (-newcp.z) * c3) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 elbowrproject = new Vector3(c1 + (lambdaaux * newcp.x), c2 + (lambdaaux * newcp.y), c3 + (lambdaaux * newcp.z));
                    float shoulderXr = body.Joints[JointType.ShoulderRight].Position.X;
                    float shoulderYr = body.Joints[JointType.ShoulderRight].Position.Y;
                    float shoulderZr = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambdaaux1 = (((-newcp.x) * shoulderXr + (-newcp.y) * shoulderYr + (-newcp.z) * shoulderZr) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 shoulderproject = new Vector3(shoulderXr + (lambdaaux1 * newcp.x), shoulderYr + (lambdaaux1 * newcp.y), shoulderZr + (lambdaaux1 * newcp.z));
                    Vector3 vectorelbowshould = new Vector3(shoulderproject.x - elbowrproject.x, shoulderproject.y - elbowrproject.y, shoulderproject.z - elbowrproject.z);
                    float angleFlex = Vector3.Angle(auxvect2, vectorelbowshould);
                    


                    
                    //Debug.Log("MAX ELEMENT OF THE ARRAY! " + Mathf.Max(RightFlex) + " " + RightFlex.Max());


                    //left side projection and result
                    float lambdaaux2 = (((-newcp.x) * c1l + (-newcp.y) * c2l + (-newcp.z) * c3l) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 elbowrprojectl = new Vector3(c1l + (lambdaaux2 * newcp.x), c2l + (lambdaaux2 * newcp.y), c3l + (lambdaaux2 * newcp.z));
                    float shoulderXl = body.Joints[JointType.ShoulderLeft].Position.X;
                    float shoulderYl = body.Joints[JointType.ShoulderRight].Position.Y;
                    float shoulderZl = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambdaaux3 = (((-newcp.x) * shoulderXl + (-newcp.y) * shoulderYl + (-newcp.z) * shoulderZl) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 shoulderprojectl = new Vector3(shoulderXl + (lambdaaux3 * newcp.x), shoulderYl + (lambdaaux3 * newcp.y), shoulderZl + (lambdaaux3 * newcp.z));
                    Vector3 vectorelbowshouldl = new Vector3(shoulderprojectl.x - elbowrprojectl.x, shoulderprojectl.y - elbowrprojectl.y, shoulderprojectl.z - elbowrprojectl.z);
                    float angleFlexl = Vector3.Angle(auxvect2, vectorelbowshouldl);
                    float anglecorrectl = 180 - angleFlexl;
                    //Debug.Log("Angle Left Flex:" + angleFlexl);
                    StartCoroutine(Temporizador(angleFlex, angleFlexl));


                    //File results
                    // File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", " Angle Right Flex: " + angleFlex + "  Angle Left Flex: " + angleFlexl);
                    // File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);
                }

                // ::::::::::::::::::::   ABD   :::::::::::::::::::: //

                if (ABDUP==true)
                {

                    //variables
                    float x1 = body.Joints[JointType.ShoulderRight].Position.X;
                    float y1 = body.Joints[JointType.ShoulderRight].Position.Y;
                    float z1 = body.Joints[JointType.ShoulderRight].Position.Z;
                    Vector3 shoulderright = new Vector3(x1, y1, z1);
                    float x2 = body.Joints[JointType.ShoulderLeft].Position.X;
                    float y2 = body.Joints[JointType.ShoulderLeft].Position.Y;
                    float z2 = body.Joints[JointType.ShoulderLeft].Position.Z;
                    Vector3 shoulderleft = new Vector3(x2, y2, z2);
                    float x3 = body.Joints[JointType.SpineMid].Position.X;
                    float y3 = body.Joints[JointType.SpineMid].Position.Y;
                    float z3 = body.Joints[JointType.SpineMid].Position.Z;
                    Vector3 spinemidv = new Vector3(x3, y3, z3);
                    float c1 = body.Joints[JointType.ElbowRight].Position.X;
                    float c2 = body.Joints[JointType.ElbowRight].Position.Y;
                    float c3 = body.Joints[JointType.ElbowRight].Position.Z;
                    Vector3 elbowrightv = new Vector3(c1, c2, c3);

                    //plane
                    Vector3 vector1 = new Vector3(x2 - x1, y2 - y1, z2 - z1);
                    Vector3 vector2 = new Vector3(x3 - x1, y3 - y1, z3 - z1);
                    Vector3 cp;
                    cp = Vector3.Cross(vector1, vector2);
                    float d = cp.x * (-x1) + cp.y * (-y1) + cp.z * (-z1);

                    //right side projection and result
                    float lambda = (((-cp.x) * c1 + (-cp.y) * c2 + (-cp.z) * c3) - d) / ((cp.x) * (cp.x) + (cp.y) * (cp.y) + (cp.z) * (cp.z));
                    float valor1 = (-cp.x) * c1 + (-cp.y) * c2 + (-cp.z) * c3;
                    float valor2 = (cp.x * cp.x + cp.y * cp.y + cp.z * cp.z);
                    Vector3 cprojected = new Vector3(c1 + (lambda * cp.x), c2 + (lambda * cp.y), c3 + (lambda * cp.z));
                    Vector3 vectorfinalelbow = new Vector3(cprojected.x - x1, cprojected.y - y1, cprojected.z - z1);
                    float u1 = body.Joints[JointType.SpineBase].Position.X;
                    float u2 = body.Joints[JointType.SpineBase].Position.Y;
                    float u3 = body.Joints[JointType.SpineBase].Position.Z;
                    float lambda3 = (((-cp.x * u1 - cp.y * u2 - cp.z * u3) - d) / (cp.x * cp.x + cp.y * cp.y + cp.z * cp.z));
                    Vector3 spinebaseproj = new Vector3(u1 + lambda3 * cp.x, u2 + lambda3 * cp.y, u3 + lambda3 * cp.z);
                    Vector3 vectorhorizproj = new Vector3(x3 - spinebaseproj.x, y3 - spinebaseproj.y, z3 - spinebaseproj.z);
                    float angleABDfinal = Vector3.Angle(vectorfinalelbow, vectorhorizproj);
                    float correct = 180 - angleABDfinal;
                    //Debug.Log("Angle ABD right = " + correct);


                    //Left side projection and result
                    float c1l = body.Joints[JointType.ElbowLeft].Position.X;
                    float c2l = body.Joints[JointType.ElbowLeft].Position.Y;
                    float c3l = body.Joints[JointType.ElbowLeft].Position.Z;
                    float lambdal = (((-cp.x) * c1l + (-cp.y) * c2l + (-cp.z) * c3l) - d) / ((cp.x) * (cp.x) + (cp.y) * (cp.y) + (cp.z) * (cp.z));
                    float valor1l = (-cp.x) * c1l + (-cp.y) * c2l + (-cp.z) * c3l;
                    float valor2l = (cp.x * cp.x + cp.y * cp.y + cp.z * cp.z);
                    Vector3 clprojected = new Vector3(c1l + (lambdal * cp.x), c2l + (lambdal * cp.y), c3l + (lambdal * cp.z));
                    Vector3 clandelbowleft = new Vector3(clprojected.x - x2, clprojected.y - y2, clprojected.z - z2);
                    float angleABDlfinal = Vector3.Angle(clandelbowleft, vectorhorizproj);
                    float correctl = 180 - angleABDlfinal;
                    // Debug.Log("Angle Abd Left: " + correctl);

                    //File results

                    //File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", " Angle Right ABD: " + correct + "  Angle Left ABD: " + correctl);
                    //File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);

                    StartCoroutine(TemporizadorABDUP(correct, correctl));
                    //flex
                    //new plane
                    Vector3 normalvect = new Vector3(cp.x, cp.y, cp.z);
                    Vector3 auxvect2 = new Vector3(x3 - u1, y3 - u2, z3 - u3);
                    Vector3 newcp;
                    newcp = Vector3.Cross(normalvect, auxvect2);
                    float daux = newcp.x * (-x3) + newcp.y * (-y3) + newcp.z * (-z3);
                    //elbow right point and projection
                    float lambdaaux = (((-newcp.x) * c1 + (-newcp.y) * c2 + (-newcp.z) * c3) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 elbowrproject = new Vector3(c1 + (lambdaaux * newcp.x), c2 + (lambdaaux * newcp.y), c3 + (lambdaaux * newcp.z));
                    //shoulder right point and projection
                    float shoulderXr = body.Joints[JointType.ShoulderRight].Position.X;
                    float shoulderYr = body.Joints[JointType.ShoulderRight].Position.Y;
                    float shoulderZr = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambdaaux1 = (((-newcp.x) * shoulderXr + (-newcp.y) * shoulderYr + (-newcp.z) * shoulderZr) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 shoulderproject = new Vector3(shoulderXr + (lambdaaux1 * newcp.x), shoulderYr + (lambdaaux1 * newcp.y), shoulderZr + (lambdaaux1 * newcp.z));

                    Vector3 vectorelbowshould = new Vector3(shoulderproject.x - elbowrproject.x, shoulderproject.y - elbowrproject.y, shoulderproject.z - elbowrproject.z);

                    float angleFlex = Vector3.Angle(auxvect2, vectorelbowshould);
                    //Debug.Log("Angle flex right:" + angleFlex);

                    //left
                    float lambdaaux2 = (((-newcp.x) * c1l + (-newcp.y) * c2l + (-newcp.z) * c3l) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 elbowrprojectl = new Vector3(c1l + (lambdaaux2 * newcp.x), c2l + (lambdaaux2 * newcp.y), c3l + (lambdaaux2 * newcp.z));
                    //shoulder left point and projection
                    float shoulderXl = body.Joints[JointType.ShoulderLeft].Position.X;
                    float shoulderYl = body.Joints[JointType.ShoulderRight].Position.Y;
                    float shoulderZl = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambdaaux3 = (((-newcp.x) * shoulderXl + (-newcp.y) * shoulderYl + (-newcp.z) * shoulderZl) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 shoulderprojectl = new Vector3(shoulderXl + (lambdaaux3 * newcp.x), shoulderYl + (lambdaaux3 * newcp.y), shoulderZl + (lambdaaux3 * newcp.z));
                    Vector3 vectorelbowshouldl = new Vector3(shoulderprojectl.x - elbowrprojectl.x, shoulderprojectl.y - elbowrprojectl.y, shoulderprojectl.z - elbowrprojectl.z);
                    float angleFlexl = Vector3.Angle(auxvect2, vectorelbowshouldl);
                   // Debug.Log("Angle flex no correction left:" + angleFlexl);


                }

                // ::::::::::::::::::::   ELBOW EXTENSION   :::::::::::::::::::: //  


                if (Elbow==true)
                {
                    //variables
                    float shoulderXr = body.Joints[JointType.ShoulderRight].Position.X;
                    float shoulderYr = body.Joints[JointType.ShoulderRight].Position.Y;
                    float shoulderZr = body.Joints[JointType.ShoulderRight].Position.Z;
                    float relbowX = body.Joints[JointType.ElbowRight].Position.X;
                    float relbowY = body.Joints[JointType.ElbowRight].Position.Y;
                    float relbowZ = body.Joints[JointType.ElbowRight].Position.Z;
                    float wristXr = body.Joints[JointType.WristRight].Position.X;
                    float wristYr = body.Joints[JointType.WristRight].Position.Y;
                    float wristZr = body.Joints[JointType.WristRight].Position.Z;
                    float shoulderXl = body.Joints[JointType.ShoulderLeft].Position.X;
                    float shoulderYl = body.Joints[JointType.ShoulderLeft].Position.Y;
                    float shoulderZl = body.Joints[JointType.ShoulderLeft].Position.Z;
                    float lelbowX = body.Joints[JointType.ElbowLeft].Position.X;
                    float lelbowY = body.Joints[JointType.ElbowLeft].Position.Y;
                    float lelbowZ = body.Joints[JointType.ElbowLeft].Position.Z;
                    float wristXl = body.Joints[JointType.WristLeft].Position.X;
                    float wristYl = body.Joints[JointType.WristLeft].Position.Y;
                    float wristZl = body.Joints[JointType.WristLeft].Position.Z;

                    //right side results
                    Vector3 vecSandEr = new Vector3(shoulderXr - relbowX, shoulderYr - relbowY, shoulderZr - relbowZ);
                    Vector3 vecWandEr = new Vector3(wristXr - relbowX, wristYr - relbowY, wristZr - relbowZ);
                    float angleElbowr = Vector3.Angle(vecSandEr, vecWandEr);
                    Debug.Log("New angle right elbow: " + angleElbowr);

                    //left side results
                    Vector3 vecSandEl = new Vector3(shoulderXl - lelbowX, shoulderYl - lelbowY, shoulderZl - lelbowZ);
                    Vector3 vecWandEl = new Vector3(wristXl - lelbowX, wristYl - lelbowY, wristZl - lelbowZ);
                    float angleElbowl = Vector3.Angle(vecSandEl, vecWandEl);
                    Debug.Log("New angle left elbow: " + angleElbowl);

                    //File results
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", " Right Elbow Extension: " + angleElbowr +
                        "  Left Elbow Extension: " + angleElbowl);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);

                }

                // ::::::::::::::::::::   SYMMETRY   :::::::::::::::::::: //     

                if (Input.GetKeyDown(KeyCode.C))
                {
                    //variables
                    float relbowX = body.Joints[JointType.ElbowRight].Position.X;
                    float relbowY = body.Joints[JointType.ElbowRight].Position.Y;
                    float relbowZ = body.Joints[JointType.ElbowRight].Position.Z;
                    float lelbowX = body.Joints[JointType.ElbowLeft].Position.X;
                    float lelbowY = body.Joints[JointType.ElbowLeft].Position.Y;
                    float lelbowZ = body.Joints[JointType.ElbowLeft].Position.Z;
                    float wristXr = body.Joints[JointType.WristRight].Position.X;
                    float wristYr = body.Joints[JointType.WristRight].Position.Y;
                    float wristZr = body.Joints[JointType.WristRight].Position.Z;
                    float wristXl = body.Joints[JointType.WristLeft].Position.X;
                    float wristYl = body.Joints[JointType.WristLeft].Position.Y;
                    float wristZl = body.Joints[JointType.WristLeft].Position.Z;
                    float shoulderXr = body.Joints[JointType.ShoulderRight].Position.X;
                    float shoulderYr = body.Joints[JointType.ShoulderRight].Position.Y;
                    float shoulderZr = body.Joints[JointType.ShoulderRight].Position.Z;
                    float shoulderXl = body.Joints[JointType.ShoulderLeft].Position.X;
                    float shoulderYl = body.Joints[JointType.ShoulderLeft].Position.Y;
                    float shoulderZl = body.Joints[JointType.ShoulderLeft].Position.Z;
                    float difElbows = Mathf.Abs(relbowY - lelbowY) * 100;
                    float difWrists = Mathf.Abs(wristYr - wristYl) * 100;
                    float difShoulders = Math.Abs(shoulderYr - shoulderYl) * 100;

                    float difElbowsZ = Mathf.Abs(relbowZ - lelbowZ) * 100;
                    float difWristsZ = Mathf.Abs(wristZr - wristZl) * 100;

                    Debug.Log("Elbow Right Heigh = " + relbowY + "  Elbow Left Heigh = " + lelbowY);
                    Debug.Log("Wrist Right Heigh = " + wristYr + "  Wrist Left Heigh = " + wristYl);
                    Debug.Log("Shoulder Right Heigh = " + shoulderYr + "  Shoulder Left Height = " + shoulderYl);
                    Debug.Log("Elbow Right Depth = " + relbowZ + "  Elbow Left Depth = " + lelbowZ);
                    Debug.Log("Wrist Right Depth = " + wristZr + "  Wrist Left Depth = " + wristZl);
                    Debug.Log("Shoulder Right Depth = " + shoulderZr + "  Shoulder Left Depth = " + shoulderZl);

                    Debug.Log("Diference between wrists: " + difWrists);
                    Debug.Log("Diference between shoulders: " + difShoulders);
                    Debug.Log("Diference between elbows: " + difElbows);
                    Debug.Log("hola");
                    //results


                    //File results
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Height Difference between wrists: " + difWrists);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Height Difference between elbows " + difElbows);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Height Difference between shoulders " + difShoulders);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Depth Difference between wrists " + difWristsZ);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Depth Difference between elbows " + difElbowsZ);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);



                }

                // ::::::::::::::::::::   WRIST EXTENSION   :::::::::::::::::::: //   

                if (Input.GetKeyDown(KeyCode.X))
                {
                    //variables
                    float wristXr = body.Joints[JointType.WristRight].Position.X;
                    float wristYr = body.Joints[JointType.WristRight].Position.Y;
                    float wristZr = body.Joints[JointType.WristRight].Position.Z;
                    float wristXl = body.Joints[JointType.WristLeft].Position.X;
                    float wristYl = body.Joints[JointType.WristLeft].Position.Y;
                    float wristZl = body.Joints[JointType.WristLeft].Position.Z;
                    float handtipXr = body.Joints[JointType.HandTipRight].Position.X;
                    float handtipYr = body.Joints[JointType.HandTipRight].Position.Y;
                    float handtipZr = body.Joints[JointType.HandTipRight].Position.Z;
                    float handtipXl = body.Joints[JointType.HandTipLeft].Position.X;
                    float handtipYl = body.Joints[JointType.HandTipLeft].Position.Y;
                    float handtipZl = body.Joints[JointType.HandTipLeft].Position.Z;
                    float relbowX = body.Joints[JointType.ElbowRight].Position.X;
                    float relbowY = body.Joints[JointType.ElbowRight].Position.Y;
                    float relbowZ = body.Joints[JointType.ElbowRight].Position.Z;
                    float lelbowX = body.Joints[JointType.ElbowLeft].Position.X;
                    float lelbowY = body.Joints[JointType.ElbowLeft].Position.Y;
                    float lelbowZ = body.Joints[JointType.ElbowLeft].Position.Z;

                    //right side results
                    Vector3 vecTipandHandr = new Vector3(handtipXr - wristXr, handtipYr - wristYr, handtipZr - wristZr);
                    Vector3 vecElbandHandr = new Vector3(relbowX - wristXr, relbowY - wristYr, relbowZ - wristZr);
                    float angleWristr = Vector3.Angle(vecTipandHandr, vecElbandHandr);
                    Debug.Log("New right wrist extension:" + angleWristr);

                    //left side results
                    Vector3 vecTipandHandl = new Vector3(handtipXl - wristXl, handtipYl - wristYl, handtipZl - wristZl);
                    Vector3 vecElbandHandl = new Vector3(lelbowX - wristXl, lelbowY - wristYl, lelbowZ - wristZl);
                    float angleWristl = Vector3.Angle(vecTipandHandl, vecElbandHandl);
                    Debug.Log("New left wrist extension:" + angleWristl);

                    //File results
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Right Wrist Extension: " + angleWristr
                        + "  Left Wrist Extension" + angleWristl);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);
                }

                // ::::::::::::::::::::   ABD HORIZONTAL   :::::::::::::::::::: //  

                if (Input.GetKeyDown(KeyCode.D))
                {
                    //variables
                    float x1 = body.Joints[JointType.ShoulderRight].Position.X;
                    float y1 = body.Joints[JointType.ShoulderRight].Position.Y;
                    float z1 = body.Joints[JointType.ShoulderRight].Position.Z;
                    float x2 = body.Joints[JointType.ShoulderLeft].Position.X;
                    float y2 = body.Joints[JointType.ShoulderLeft].Position.Y;
                    float z2 = body.Joints[JointType.ShoulderLeft].Position.Z;
                    float c1 = body.Joints[JointType.ElbowRight].Position.X;
                    float c2 = body.Joints[JointType.ElbowRight].Position.Y;
                    float c3 = body.Joints[JointType.ElbowRight].Position.Z;
                    float c1l = body.Joints[JointType.ElbowLeft].Position.X;
                    float c2l = body.Joints[JointType.ElbowLeft].Position.Y;
                    float c3l = body.Joints[JointType.ElbowLeft].Position.Z;
                    float n1 = body.Joints[JointType.Neck].Position.X;
                    float n2 = body.Joints[JointType.Neck].Position.Y;
                    float n3 = body.Joints[JointType.Neck].Position.Z;

                    //ref vectors and planes
                    Vector3 vectorRef = new Vector3(x1 - x2, y1 - y2, z1 - z2);
                    Vector3 vecShrNe = new Vector3(x1 - n1, y1 - n2, z1 - n3);
                    Vector3 vecShlNe = new Vector3(x2 - n1, y2 - n2, z2 - n3);
                    Vector3 plane;
                    plane = Vector3.Cross(vecShrNe, vecShlNe);
                    float d1 = plane.x * (-x1) + plane.y * (-y1) + plane.z * (-z1);

                    //right side results
                    float lambda = (((-plane.x) * c1 + (-plane.y) * c2 + (-plane.z) * c3) - d1) / ((plane.x) * (plane.x) + (plane.y) * (plane.y) + (plane.z) * (plane.z));
                    Vector3 elbowrproject = new Vector3(c1 + (lambda * plane.x), c2 + (lambda * plane.y), c3 + (lambda * plane.z));
                    Vector3 vecElShr = new Vector3(elbowrproject.x - x1, elbowrproject.y - y1, elbowrproject.z - z1);
                    float angleABDhoriz = Vector3.Angle(vectorRef, vecElShr);
                    Debug.Log("Angle Right ABD Horiz: " + angleABDhoriz);

                    //left side results
                    float lambda2 = (((-plane.x) * c1l + (-plane.y) * c2l + (-plane.z) * c3l) - d1) / ((plane.x) * (plane.x) + (plane.y) * (plane.y) + (plane.z) * (plane.z));
                    Vector3 elbowrprojectl = new Vector3(c1l + (lambda2 * plane.x), c2l + (lambda2 * plane.y), c3l + (lambda2 * plane.z));
                    Vector3 vecElShl = new Vector3(elbowrprojectl.x - x2, elbowrprojectl.y - y2, elbowrprojectl.z - z2);
                    float angleABDhorizl = Vector3.Angle(vectorRef, vecElShl);
                    Debug.Log("Angle Left ABD Horiz: " + angleABDhorizl);

                    //File results
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Right Horizontal ABD: " + angleABDhoriz
                        + "  Left Horizontal ABD" + angleABDhorizl);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);

                }


                // ::::::::::::::::::::   EXTERNAL ROTATION   :::::::::::::::::::: //  

                if (Input.GetKeyDown(KeyCode.F))
                {
                    //variables
                    float x1 = body.Joints[JointType.ShoulderRight].Position.X;
                    float y1 = body.Joints[JointType.ShoulderRight].Position.Y;
                    float z1 = body.Joints[JointType.ShoulderRight].Position.Z;
                    float x2 = body.Joints[JointType.ShoulderLeft].Position.X;
                    float y2 = body.Joints[JointType.ShoulderLeft].Position.Y;
                    float z2 = body.Joints[JointType.ShoulderLeft].Position.Z;
                    float x3 = body.Joints[JointType.SpineMid].Position.X;
                    float y3 = body.Joints[JointType.SpineMid].Position.Y;
                    float z3 = body.Joints[JointType.SpineMid].Position.Z;
                    float u1 = body.Joints[JointType.SpineBase].Position.X;
                    float u2 = body.Joints[JointType.SpineBase].Position.Y;
                    float u3 = body.Joints[JointType.SpineBase].Position.Z;
                    float c1 = body.Joints[JointType.ElbowRight].Position.X;
                    float c2 = body.Joints[JointType.ElbowRight].Position.Y;
                    float c3 = body.Joints[JointType.ElbowRight].Position.Z;
                    float c1l = body.Joints[JointType.ElbowLeft].Position.X;
                    float c2l = body.Joints[JointType.ElbowLeft].Position.Y;
                    float c3l = body.Joints[JointType.ElbowLeft].Position.Z;
                    float w1r = body.Joints[JointType.WristRight].Position.X;
                    float w2r = body.Joints[JointType.WristRight].Position.Y;
                    float w3r = body.Joints[JointType.WristRight].Position.Z;
                    float w1l = body.Joints[JointType.WristLeft].Position.X;
                    float w2l = body.Joints[JointType.WristLeft].Position.Y;
                    float w3l = body.Joints[JointType.WristLeft].Position.Z;

                    //ref vectors and planes
                    Vector3 vector1 = new Vector3(x2 - x1, y2 - y1, z2 - z1);
                    Vector3 vector2 = new Vector3(x3 - x1, y3 - y1, z3 - z1);
                    Vector3 cp;
                    cp = Vector3.Cross(vector1, vector2);
                    Vector3 normalvect = new Vector3(cp.x, cp.y, cp.z);
                    Vector3 auxvect2 = new Vector3(x3 - u1, y3 - u2, z3 - u3);
                    Vector3 newcp;
                    newcp = Vector3.Cross(normalvect, auxvect2);
                    float daux = newcp.x * (-x3) + newcp.y * (-y3) + newcp.z * (-z3);

                    //right projections and results
                    float lambdaaux = (((-newcp.x) * c1 + (-newcp.y) * c2 + (-newcp.z) * c3) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 elbowrproject = new Vector3(c1 + (lambdaaux * newcp.x), c2 + (lambdaaux * newcp.y), c3 + (lambdaaux * newcp.z));
                    float lambdaaux2 = (((-newcp.x) * w1r + (-newcp.y) * w2r + (-newcp.z) * w3r) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 wristrproject = new Vector3(w1r + (lambdaaux2 * newcp.x), w2r + (lambdaaux2 * newcp.y), w3r + (lambdaaux2 * newcp.z));
                    Vector3 vecElbWrisr = new Vector3(elbowrproject.x - wristrproject.x, elbowrproject.y - wristrproject.y, elbowrproject.z - wristrproject.z);
                    float angleExtRotr = Vector3.Angle(vecElbWrisr, auxvect2);
                    float angleCorrec = angleExtRotr - 90;
                    Debug.Log("Angle Ext Rot Right: " + angleCorrec);

                    //left projections and results
                    float lambdaauxl = (((-newcp.x) * c1l + (-newcp.y) * c2l + (-newcp.z) * c3l) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 elbowprojectl = new Vector3(c1l + (lambdaauxl * newcp.x), c2l + (lambdaauxl * newcp.y), c3l + (lambdaauxl * newcp.z));
                    float lambdaaux2l = (((-newcp.x) * w1l + (-newcp.y) * w2l + (-newcp.z) * w3l) - daux) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 wristrprojectl = new Vector3(w1l + (lambdaaux2l * newcp.x), w2l + (lambdaaux2l * newcp.y), w3l + (lambdaaux2l * newcp.z));
                    Vector3 vecElbWrisl = new Vector3(elbowprojectl.x - wristrprojectl.x, elbowprojectl.y - wristrprojectl.y, elbowprojectl.z - wristrprojectl.z);
                    float angleExtRotl = Vector3.Angle(vecElbWrisl, auxvect2);
                    float angleCorrecl = angleExtRotl - 90;
                    Debug.Log("Angle Ext Rot Left: " + angleCorrecl);

                    //File results
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Right External Rotation: " + angleCorrec
                        + "  Right External Rotation" + angleCorrecl);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);


                }

                // ::::::::::::::::::::   TOUCHING SHOULDERS YER OR NO  :::::::::::::::::::: //   se necesita modificar para tocar la rampa 
                if (Input.GetKeyDown(KeyCode.G))
                {
                    //variables
                    float handtipYr = body.Joints[JointType.HandTipRight].Position.Y;
                    float handtipZr = body.Joints[JointType.HandTipRight].Position.Z;
                    float shoulderYr = body.Joints[JointType.ShoulderRight].Position.Y;
                    float shoulderZr = body.Joints[JointType.ShoulderRight].Position.Z;
                    float handtipYl = body.Joints[JointType.HandTipLeft].Position.Y;
                    float handtipZl = body.Joints[JointType.HandTipLeft].Position.Z;
                    float shoulderYl = body.Joints[JointType.HandTipLeft].Position.Y;
                    float shoulderZl = body.Joints[JointType.HandTipLeft].Position.Z;

                    //differences 
                    float touchYl = Mathf.Abs(shoulderYl - handtipYl) * 100;
                    float touchZl = Mathf.Abs(shoulderZl - handtipZl) * 100;
                    float touchYr = Mathf.Abs(shoulderYr - handtipYr) * 100;
                    float touchZr = Mathf.Abs(shoulderZr - handtipZr) * 100;

                    //right side results and file results
                    if (touchYr < 5 && touchZr < 15)
                    {
                        Debug.Log("Right Hand touch Right Shoulder");
                        //File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Right Hand Touch Right Shoulder ");
                        //File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);
                    }
                    else
                    {
                        Debug.Log("Right Hand don't touch Right Shoulder");
                        //File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Right Hand Don't Touch Right Shoulder ");
                        //File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);
                    }

                    //left side results and file results
                    if (touchYl < 5 && touchZl < 15)
                    {
                        Debug.Log("Left Hand touch Left Shoulder");
                        // File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Left Hand Touch Left Shoulder ");
                        //File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);
                    }
                    else
                    {
                        Debug.Log("Left Hand don't touch Left Shoulder");
                        // File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Left Hand Don't Touch Left Shoulder ");
                        //File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);
                    }

                }

                //::::::::::::   HEAD MOVEMENT  :::::::::::::    revisarrrrrrrrrrrrrrrrrrrrrrr!!!

                if (Input.GetKeyDown(KeyCode.V))
                {
                    //variables
                    float headXrr = body.Joints[JointType.Head].Position.X;
                    float headYrr = body.Joints[JointType.Head].Position.Y;
                    float headZrr = body.Joints[JointType.Head].Position.Z;
                    float rtShoulderXr = body.Joints[JointType.ShoulderRight].Position.X;
                    float rtShoulderYr = body.Joints[JointType.SpineShoulder].Position.Y;
                    float rtShoulderZr = body.Joints[JointType.SpineShoulder].Position.Z;
                    float rtShoulderXl = body.Joints[JointType.ShoulderLeft].Position.X;
                    float rtShoulderYl = body.Joints[JointType.ShoulderLeft].Position.Y;
                    float rtShoulderZl = body.Joints[JointType.ShoulderLeft].Position.Z;
                    float neeckXr = body.Joints[JointType.Neck].Position.X;
                    float neeckYr = body.Joints[JointType.Neck].Position.Y;
                    float neeckZr = body.Joints[JointType.Neck].Position.Z;

                    //right side results
                    Vector3 vecHeandNeckr = new Vector3(headXrr - neeckXr, headYrr - neeckYr, headYrr - neeckZr);
                    Vector3 vecShoandNeckr = new Vector3(rtShoulderXr - neeckXr, rtShoulderYr - neeckYr, rtShoulderZr - neeckZr);
                    float neckMover = Vector3.Angle(vecHeandNeckr, vecShoandNeckr);
                    Debug.Log("Neck movement degrees right part" + neckMover);

                    //left side results
                    Vector3 vecShoandNeckl = new Vector3(rtShoulderXl - neeckXr, rtShoulderYl - neeckYr, rtShoulderZl - neeckZr);
                    float neckMovel = Vector3.Angle(vecHeandNeckr, vecShoandNeckl);
                    Debug.Log("Neck movement degrees left part" + neckMovel);


                    //File results
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", "Angle Head - Right Shoulder : " + neckMover
                        + "  Angle Head - Left Shoulder" + neckMovel);
                    File.AppendAllText(@"C:\Users\Aleix\Desktop\Results.txt", Environment.NewLine);


                }






            
        }
    }

    }


    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.SetVertexCount(2);
            lr.material = BoneMaterial;
            lr.SetWidth(0.0f, 0.0f);

            jointObj.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }

        return body;


    }

    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;

            if (_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            Transform jointObj = bodyObject.transform.Find(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if (targetJoint.HasValue)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }
        }
    }

    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
            case Kinect.TrackingState.Tracked:
                return Color.green;

            case Kinect.TrackingState.Inferred:
                return Color.red;

            default:
                return Color.black;
        }
    }

    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {

        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}






