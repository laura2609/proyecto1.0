﻿using UnityEngine;
using System;
using System.IO.Ports; //incluimos el namespace Sustem.IO.Ports
using System.Collections;
using System.Collections.Generic;



public class Resis_Sensor : MonoBehaviour
{

    SerialPort serialPort = new SerialPort("COM9", 9600); //Inicializamos el puerto serie
    //SerialPort serialPort = new SerialPort();
    private int score = 0;
    private int cont = 0;
    void Start()
    {
        Debug.Log("New sensor");
        Debug.Log(this);

        serialPort.Open(); //Abrimos una nueva conexión de puerto serie
        serialPort.ReadTimeout = 1; //Establecemos el tiempo de espera cuando una operación de lectura no finaliza

    }

    public int getScore()
    {
        return score;
    }

    void Update()
    {
        if (serialPort.IsOpen) //comprobamos que el puerto esta abierto
        {
            try //utilizamos el bloque try/catch para detectar una posible excepción.
            {

                string value = serialPort.ReadLine(); //leemos una linea del puerto serie y la almacenamos en un string
                print(value); //impresion de la linea leida para verificar que leemos el dato que manda nuestro Arduino
                              //string[] vec6 = value.Split(','); //Separamos el String leido valiendonos 
                              //                                  //de las comas y almacenamos los valores en un array.
                int val = int.Parse(value);

                if (val > 0)
                {
                    if (cont == 0)
                    {
                        score = score + 1;
                    };
                    cont = 1;
                }
                else
                { cont = 0; };
                print("score=" + score);


            }

            catch (System.Exception ex)
            {
                ex = new System.Exception();

            }


        }

    }

}
