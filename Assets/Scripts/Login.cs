﻿//LIBRERIAS
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Script que permite el Login de un Usuario (al darle al botón Login en la escaena GlobalAccountSystem): poniendo los datos del Usuario (DNI y contraseña), 
/// se entra en el Perfil del usuario (mediante el script Login.php) y se pasa a la siguiente escena "Pacients" para poder insertar pacientes a ese Usuario
/// </summary>
public class Login : MonoBehaviour
{

    //DECLARACIÓN DE VARIABLES

    public GameObject UseerName;
    private Text UserName;
    public GameObject Paassword;
    private Text Password;

    public Text Mensaje=null;

    string LoginURL = "localhost/users/Login.php";

    // Use this for initialization
    void Start()
    {
        //TRANSFORMACIN VARIABLE (GameObject a Text)
        UserName = UseerName.GetComponent<Text>();
        Password = Paassword.GetComponent<Text>();
    }

 

    public void ButtonClick()
    {
        //INICIO CORUTINA 
        StartCoroutine(LoginToDB(UserName.text, Password.text));
   
    }

    IEnumerator LoginToDB(string UserName, string Password)
    {
        WWWForm form = new WWWForm();


        form.AddField("UserNamePost", UserName);
        form.AddField("PasswordPost", Password);

        WWW www = new WWW(LoginURL, form);

        yield return www;
        
        Debug.Log(www.text);
        //Devuelve  los datos recibidos del PHP
        Mensaje.text= www.text;
        //Si recibe la frase "Inicio de sesión correcto" procede a iniciar sesión
        if ( www.text == "Inicio de sesión correcto")
        {
            SceneManager.LoadScene("Pacients", LoadSceneMode.Single);
        }
    }

}