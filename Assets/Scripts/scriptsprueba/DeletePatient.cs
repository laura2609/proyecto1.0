﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic; //
using System;
using System.Text;
using UnityEngine.EventSystems;
using System.Text.RegularExpressions;
using System.Linq;
//Escena Pacientes, boton EliminarPaciente. Sirve para coger los datos del paciente para eliminar y enviarlos al php.
public class DeletePatient : MonoBehaviour
{


    public GameObject PatientNames;
    public Text PatientName;
    private string DoctoresName;
    public GameObject DDNI;
    private Text DNI;
    public Button myButton;//boton Refresh //Dado que no puede Eliminar y Refrescar pacientes a la vez se le pone un pequeño temporizador de 0.2 Seg

    string DeletePatientURL = "localhost/users/DeletePatient.php";

    // Use this for initialization
    void Start()
    {

        DNI = DDNI.GetComponent<Text>();
  
        
    
}


    public void Update()
    {

        PatientName = PatientNames.GetComponent<Text>();
        DoctoresName = DNI.text;

    }
    public void ButtonClick()
    {
        StartCoroutine(Lista1(PatientName.text, DoctoresName));
        
    }

IEnumerator Lista1(string PatientName, string DoctoresName)
{
        WWWForm form = new WWWForm();

        form.AddField("PatientNamePost", PatientName);
        form.AddField("DoctorNamePost", DoctoresName);
        WWW www = new WWW(DeletePatientURL, form);

        yield return www;
        //Debug.Log(www.text);
        StartCoroutine(Temporizador());//llama a la corutina Temporizador


    }
    IEnumerator Temporizador()//Dado que no puede Eliminar y Refrescar pacientes a la vez se le pone un pequeño temporizador de 0.2 Seg
    {
        yield return new WaitForSeconds(0.2f);
        myButton.onClick.Invoke();

    }

}
