﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
//escena Pacientes. Ubicada dentro el prefab (SampleButton),Funcion:Util para que en el Script shopScroll list pueda aparecer el boton tantas veces como pacientes haya
public class SampleButton : MonoBehaviour
{

    public Button buttonComponent;
    public Text nameLabel;
   
    private Item item;
    private ShopScrollList scrollList;
    public GameObject panel;
    public Text NameText;
    public Text NameanguloText;

    public void Setup(Item currentItem, ShopScrollList currentScrollList)
    {
        item = currentItem;
        nameLabel.text = item.itemName;//cambiar aqui el nombre del boton
       
        scrollList = currentScrollList;

    }
    public void openpanel()
    {
        if (panel != null)
        {
            bool isActive = panel.activeSelf;
            panel.SetActive(!isActive);
        }
    }

    public void ButtonClick()
    {
        Debug.Log("copaido");
        
        NameText.text = this.name;//
        NameanguloText.text = this.name;
        Debug.Log(NameanguloText.text);
    }

    }