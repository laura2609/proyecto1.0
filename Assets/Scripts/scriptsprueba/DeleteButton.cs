﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeleteButton : MonoBehaviour
{

    public Button buttonComponent;
    public Text nameLabel;



    private Item items;
    private ShopScrollList scrollList;

    // Use this for initialization
    /*void Start()
    {
        buttonComponent.onClick.AddListener(HandleClick);
    }*/

    public void Setup(Item currentItem, ShopScrollList currentScrollList)
    {
        items = currentItem;
        nameLabel.text = items.itemName;//cambiar aqui el nombre del boton

        scrollList = currentScrollList;

    }

    /*public void HandleClick()
    {
        scrollList.TryTransferItemToOtherShop(item);
    }*/
}