﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

//Escena Pacients, en Content. Sirve para crear una lista de botones segun el numero de pacientes que haya para un doctor determinado (que se obtiene con el script CopiaDNI, la parte de: GameObject.FindGameObjectsWithTag("DNI");)
[System.Serializable]
public class Item
{
    public string itemName;
    
}

public class ShopScrollList : MonoBehaviour
{

   public List<Item> itemList; 

    public Transform contentPanel;
    //public Transform contentPanelEliminar;
   public SimpleObjectPool buttonObjectPool;
   // public SimpleObjectPool buttonObjectPool2;


    public string[] PacientesB;
    public bool refresh;

    public Text mytext = null;
    public GameObject[] DNI;
    public string nombre;

    public void Update()
    {
    
        
    }
    private void Start()
    {
        StartCoroutine(Temporizador());//llama a la corutina Temporizador al iniciar la escena
    }

    public void ButtonClick()
    {
        
        RemoveButtons();
      

        Listado variable = GetComponent<Listado>();//en rojo pero funciona
        PacientesB = variable.Pacientes;

        for (int i = 0; i < PacientesB.Length - 1; i++) //Bucle For: crea tantos botones como splits "/n" (saltos de linea) devuelva el script Listado -1
        {
           

            // Item item = PacientesB[i];
            GameObject newButton = buttonObjectPool.GetObject();
            newButton.transform.SetParent(contentPanel);//se crean los botones en el panel "Content"

            SampleButton sampleButton = newButton.GetComponent<SampleButton>();
            sampleButton.GetComponentInChildren<Text>().text = PacientesB[i];
            sampleButton.name = PacientesB[i];//copia el nombre de los splits "/n" (saltos de linea) encima de cada botón
           /*
            GameObject newButton2 = buttonObjectPool2.GetObject();
            newButton2.transform.SetParent(contentPanelEliminar);
            newButton2.name = PacientesB[i];*/

        }
        

    }

     void RemoveButtons() //para actualizar el numero de botones, se eliminan todos y se vuelven a crear al darle al botón Refresh de la escena Pacients.
    {
        while (contentPanel.childCount > 0)
        {
            GameObject toRemove = contentPanel.transform.GetChild(0).gameObject;
            buttonObjectPool.ReturnObject(toRemove);
             //GameObject toRemove2 = contentPanelEliminar.transform.GetChild(0).gameObject;
            //buttonObjectPool2.ReturnObject(toRemove2);


        }

    }

    IEnumerator Temporizador()//Permite que a la iniciar la escena salga automaticamente la lista de pacientes sin darle al boton refresh despues de 0.5S
    {
        yield return new WaitForSeconds(0.5f);
        ButtonClick();

    }
    

}