﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class Vector4
{
    public static Quaternion ChangeQuat(this Windows.Kinect.Vector4 newvector,
                                                                Quaternion newrotation)
    {
        return Quaternion.Inverse(newrotation) *
                    new Quaternion(-newvector.X, -newvector.Y, newvector.Z, newvector.W);
    }

}