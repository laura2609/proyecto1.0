﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Script que permite el Login de un Paciente (al darle al botón Login en la escaena GlobalAccountSystem): poniendo los datos del Paciente (DNI), 
/// se entra en el Perfil del paciente (mediante el script LoginPaciente.php) y se pasa a la siguiente escena "Movement" para poder escoger los ejercicios para hacer.
/// </summary>

public class LoginPaciente : MonoBehaviour {

    public GameObject UseerName; //realmente es el DNI paciente
    private Text UserName;
    public Text Mensaje = null;


    string LoginURL = "localhost/users/LoginPaciente.php";

    // Use this for initialization
    void Start()
    {
        UserName = UseerName.GetComponent<Text>();
    }

    public void ButtonClick()
    {
        StartCoroutine(LoginToDB(UserName.text));

    }

    IEnumerator LoginToDB(string UserName)
    {
        WWWForm form = new WWWForm();
        form.AddField("DNIPost", UserName);
        WWW www = new WWW(LoginURL, form);//envia el DNI al script LoginPaciente.php

        yield return www;

        Debug.Log(www.text);
        Mensaje.text = www.text;
        if (www.text == "Inicio de sesión correcto")//comprueba si el DNI existe
        {
            //SceneManager.LoadScene("Movement", LoadSceneMode.Single); //se carga la siguiente escena (escena Movement)
            SceneManager.LoadScene(2);
        }
    }

}