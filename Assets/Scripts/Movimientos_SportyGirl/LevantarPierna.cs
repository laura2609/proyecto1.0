﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class LevantarPierna : MonoBehaviour
{

    private Animator animator;




    const int countOfDamageAnimations = 3;
    int lastDamageAnimation = -1;

    public void ButtonClick()
    {
        
        animator.SetBool("Squat", false);
        animator.SetFloat("Speed", 0f);
        animator.SetBool("Aiming", false);
        animator.SetTrigger("Izquierda_Pierna");
    }
    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    
}