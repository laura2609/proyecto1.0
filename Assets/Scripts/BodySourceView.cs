﻿using UnityEngine;
using System.Collections;
using Windows.Data;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using Windows.Kinect;
using System.Linq;
using System.IO;
using System;
using UnityEngine.UI;


public class BodySourceView : MonoBehaviour
{
    public Text elbowPrint;
    public Resis_Sensor resis_Sensor;
    public Material BoneMaterial;
    public GameObject BodySourceManager;
    public GameObject _personaje1_Unity;
    public GameObject Master;
    public GameObject Reference;
    public GameObject Hips;
    public GameObject LeftUpLeg;
    public GameObject LeftLeg;
    public GameObject RightUpLeg;
    public GameObject RightLeg;
    public GameObject Spine;
    public GameObject Spine1;
    public GameObject Spine2;
    public GameObject LeftShoulder;
    public GameObject LeftArm;
    public GameObject LeftForeArm;
    public GameObject LeftHand;
    public GameObject Neck;
    public GameObject Head;
    public GameObject RightShoulder;
    public GameObject RightArm;
    public GameObject RightForeArm;
    public GameObject RightHand;
    public GameObject LeftFoot; //
    public GameObject RightFoot; //
    public GameObject RightAnkle; //
    public GameObject LeftAnkle; //
    public float F1 = 0;
    public float F2 = 0;
    public float F3 = 0;
    public float F4 = 0;
    public Text derecha = null; //Angulo Actual Derecho
    public Text izquierda = null; //Angulo Actual Izquierdo 
    public Text derecha_1 = null;
    public Text izquierda_1 = null;
    public float maxtot = 0;
    public int cont = -1;
    public bool rep = true;
    public Text angulo = null; //Angulo_Max_Derecho
    public Text angulo2 = null; //Angulo_Max_Izquierdo
    public Text angulo3 = null; //Angulo_Max_Derecho_Rodilla
    public Text angulo4 = null; //Angulo_Max_Izquierdo_Rodilla
    public bool angulonum = false;
    public GameObject semaforo;
    public GameObject semaforo2;
    public Text copiaej = null;
    public float valor;
    public float valor2;
    public float valor3;
    public float valor4;
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;
    //public Text repeticionesnum;
    //public bool repeticiones0 = false;

// Canvax Maximos y minimos value
/// </summary>

    public bool flex = false;
    public void ButtonClick()
    {
        Debug.Log("FLEX");
        flex = true;
        StartCoroutine(Seemaforo());
   }

/// </summary>

// Mapeo "puntos" del Cuerpo Humano

private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    public void Start()
    {
        _personaje1_Unity = GameObject.Find("personaje1_Unity");
        Master = GameObject.Find("master");
        Reference = GameObject.Find("Reference");
        Hips = GameObject.Find("Hips");
        LeftUpLeg = GameObject.Find("LeftUpLeg");
        LeftLeg = GameObject.Find("LeftLeg");
        RightUpLeg = GameObject.Find("RightUpLeg");
        RightLeg = GameObject.Find("RightLeg");
        Spine = GameObject.Find("Spine");
        Spine1 = GameObject.Find("Spine1");
        Spine2 = GameObject.Find("Spine2");
        LeftShoulder = GameObject.Find("LeftShoulder");
        LeftArm = GameObject.Find("LeftArm");
        LeftForeArm = GameObject.Find("LeftForeArm");
        LeftHand = GameObject.Find("LeftHand");
        Neck = GameObject.Find("Neck");
        Head = GameObject.Find("Head");
        RightShoulder = GameObject.Find("RightShoulder");
        RightArm = GameObject.Find("RightArm");
        RightForeArm = GameObject.Find("RightForeArm");
        RightHand = GameObject.Find("RightHand");
        RightAnkle = GameObject.Find("RightAnkle"); //
        LeftAnkle = GameObject.Find("LeftAnkle");   //
        RightFoot = GameObject.Find("RightFoot");  //
        LeftFoot = GameObject.Find("LeftFoot");    //
        
    }
    IEnumerator Seemaforo()
    {
        copiaej.text = "Realice el ejercicio propuesto";
        yield return new WaitForSeconds(7);
        Destroy(semaforo);
        copiaej.text = "Ahora es su turno!";
        semaforo2.SetActive(true);
    }
    
    IEnumerator Temporizador(float AngleFlex1co, float AngleFlexlcorrec, float AngleKneeR_Co, float AngleKneeL_Co)
    {
        flex = false;
        yield return new WaitForSeconds(0.5f);// Mira angulos FLex Cada segundo miembro inferior
        MAX(AngleFlex1co, AngleFlexlcorrec, AngleKneeR_Co, AngleKneeL_Co);
        flex = true;      
    } 

    public void MAX(float AngleFlex1co, float AngleFlexlcorrec, float AngleKneeR_Co, float AngleKneeL_Co)
    {
        if (AngleFlex1co > F1)
        {
            F1 = AngleFlex1co;
        }
        if (AngleFlexlcorrec > F2)
        {
            F2 = AngleFlexlcorrec;
        }
        if (AngleKneeR_Co > F3)
        {
            F3 = AngleKneeR_Co;
        }
        if (AngleKneeL_Co > F4)
        {
            F4 = AngleKneeR_Co;
        }
        valor = AngleFlex1co;
        valor2 = AngleFlexlcorrec;
        valor3 = AngleKneeR_Co;
        valor4 = AngleKneeL_Co;
        derecha.text = ("" + AngleFlex1co);
        izquierda.text = ("" + AngleFlexlcorrec);
        derecha_1.text = ("" + AngleKneeR_Co);
        izquierda_1.text = ("" + AngleKneeL_Co);
        //Repeticiones(valor);
        angulo.text = ("" + F1);//
        angulo2.text = ("" + F2);
        angulo3.text = ("" + F3);
        angulo4.text = ("" + F4);
    }

    /*public void Repeticiones(float valor)
    {
        repeticionesnum.text = ("" + cont);
        if (valor < 20 && rep == true)
        {
            if (repeticiones0 == true)
            {
                cont = cont + 1;
            }
            
            rep = false;
            Debug.Log(cont);

            if (cont == 10)
            {
                Debug.Log("Valor maximo enviado :   " + F1);
                Debug.Log("Valor maximo enviado :   " + F2);
                Debug.Log("Valor maximo enviado :   " + F3);
                Debug.Log("Valor maximo enviado :   " + F4);
            }

        }
        if (valor > 20)
        {
            rep = true;
            repeticiones0 = true;

        }
        

    }
    */
    void Update()
    {
        elbowPrint.text = resis_Sensor.getScore().ToString();


        if (BodySourceManager == null)
        {
            Debug.Log("BSC NULL");
            return;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            Debug.Log("BSC NULL");
            return;
        }

        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            //Debug.Log("NO KINECT DATA, YOU EXIT UPDATE FUNCTION HERE");
            return;
        }

        var floorPlane = _BodyManager.FloorPlanes;
        var newrotation = Quaternion.FromToRotation(new Vector3(floorPlane.X, floorPlane.Y, floorPlane.Z), Vector3.up);


        List<ulong> trackedIds = new List<ulong>();
        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                trackedIds.Add(body.TrackingId);
            }
        }

        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);

        // First delete untracked bodies
        foreach (ulong trackingId in knownIds)
        {
            if (!trackedIds.Contains(trackingId))
            {
                
                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }
        Debug.Log("body");
        foreach (var body in data)
        {
            
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                if (!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }

                RefreshBodyObject(body, _Bodies[body.TrackingId]);

                Quaternion SpineBase = body.JointOrientations[JointType.SpineBase].Orientation.ChangeQuat(newrotation);
                Quaternion SpineMid = body.JointOrientations[JointType.SpineMid].Orientation.ChangeQuat(newrotation);
                Quaternion SpineShoulder = body.JointOrientations[JointType.SpineShoulder].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderLeft = body.JointOrientations[JointType.ShoulderLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderRight = body.JointOrientations[JointType.ShoulderRight].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowLeft = body.JointOrientations[JointType.ElbowLeft].Orientation.ChangeQuat(newrotation);
                Quaternion WristLeft = body.JointOrientations[JointType.WristLeft].Orientation.ChangeQuat(newrotation);
                Quaternion HandLeft = body.JointOrientations[JointType.HandLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowRight = body.JointOrientations[JointType.ElbowRight].Orientation.ChangeQuat(newrotation);
                Quaternion WristRight = body.JointOrientations[JointType.WristRight].Orientation.ChangeQuat(newrotation);
                Quaternion HandRight = body.JointOrientations[JointType.HandRight].Orientation.ChangeQuat(newrotation);
                Quaternion KneeLeft = body.JointOrientations[JointType.KneeLeft].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleLeft = body.JointOrientations[JointType.AnkleLeft].Orientation.ChangeQuat(newrotation);
                Quaternion KneeRight = body.JointOrientations[JointType.KneeRight].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleRight = body.JointOrientations[JointType.AnkleRight].Orientation.ChangeQuat(newrotation);
                Quaternion FootRight = body.JointOrientations[JointType.FootRight].Orientation.ChangeQuat(newrotation); //
                Quaternion FootLeft = body.JointOrientations[JointType.FootLeft].Orientation.ChangeQuat(newrotation); //
                FootRight.y = 1;
                FootRight.w = 0;
                FootLeft.y = 1;
                FootLeft.w = 0;
                Spine1.transform.rotation = SpineMid * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightArm.transform.rotation = ElbowRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightForeArm.transform.rotation = WristRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightHand.transform.rotation = HandRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftArm.transform.rotation = ElbowLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftForeArm.transform.rotation = WristLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightFoot.transform.rotation = FootRight * Quaternion.AngleAxis(0, new Vector3(0, 0, 1)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0)); // ojo -90
                LeftFoot.transform.rotation = FootLeft * Quaternion.AngleAxis(180, new Vector3(1, 0, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));//
                LeftHand.transform.rotation = HandLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightUpLeg.transform.rotation = KneeRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightLeg.transform.rotation = AnkleRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftUpLeg.transform.rotation = KneeLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftLeg.transform.rotation = AnkleLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

                var moveposition = body.Joints[JointType.SpineMid].Position;
                Master.transform.position = new Vector3(moveposition.X, moveposition.Y, -moveposition.Z);


                ////////////////////////////////////Miembro inferior////////////////////////////////////////////////////////////////////////////////////////////////////    
                // ::::::::::::::::::::::::::  HIP - KNEE EXERCISE HORIZONTAL  :::::::::::::::::::: // 

                if (Input.GetKeyDown(KeyCode.B))
                {
                    Debug.Log("Horizontal Exercise selected");
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", " ::::::   HORIZONTAL EXERCISE   :::::: "); // Guardar los datos 
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

                }
                // ::::::::::::::::::::   SYMMETRY  BODY :::::::::::::::::::: //     
                Debug.Log("Key check");
                if (Input.GetKeyDown(KeyCode.C))
                {
                    //variables
                    float KneeeX = body.Joints[JointType.KneeRight].Position.X;
                    float KneeeY = body.Joints[JointType.KneeRight].Position.Y;
                    float KneeeZ = body.Joints[JointType.KneeRight].Position.Z;
                    float lelkneeX = body.Joints[JointType.KneeLeft].Position.X;
                    float lelkneeY = body.Joints[JointType.KneeLeft].Position.Y;
                    float lelkneeZ = body.Joints[JointType.KneeLeft].Position.Z;
                    float AnkleXr = body.Joints[JointType.AnkleRight].Position.X;
                    float AnkleYr = body.Joints[JointType.AnkleRight].Position.Y;
                    float AnkleZr = body.Joints[JointType.AnkleRight].Position.Z;
                    float AnkleXl = body.Joints[JointType.AnkleLeft].Position.X;
                    float AnkleYl = body.Joints[JointType.AnkleLeft].Position.Y;
                    float AnkleZl = body.Joints[JointType.AnkleLeft].Position.Z;
                    float HipXr = body.Joints[JointType.HipRight].Position.X;
                    float HipYr = body.Joints[JointType.HipRight].Position.Y;
                    float HipZr = body.Joints[JointType.HipRight].Position.Z;
                    float HipXl = body.Joints[JointType.HipLeft].Position.X;
                    float HipYl = body.Joints[JointType.HipLeft].Position.Y;
                    float HipZl = body.Joints[JointType.HipLeft].Position.Z;
                    //results- Medicion de la simetria entre cada una de las partes 
                    float Difknee = Mathf.Abs(KneeeY - lelkneeY) * 100; //Diferencia entre Rodilla izquierda y derecha 
                    float DifAnkle = Mathf.Abs(AnkleYr - AnkleYl) * 100; //Diferencia entre Tobillo izquierdo y derecho 
                    float DifHip = Math.Abs(HipYr - HipYl) * 100; //Diferencia cadera 
                    float DifKneeZ = Mathf.Abs(KneeeZ - lelkneeZ) * 100; //Diferencia entre Rodilla derecha y izquierda 
                    float DifAnkleZ = Mathf.Abs(AnkleZr - AnkleZl) * 100; //Diferencia entre entre derecha y izquierda 


                    Debug.Log("Knee Right Heigh = " + KneeeY + "  Knee Left Heigh = " + lelkneeY); //Simetria de la rodilla 
                    Debug.Log("Ankle Right Heigh = " + AnkleYr + "  Ankle Left Heigh = " + AnkleYl); //Simetria del tobillo
                    Debug.Log("Hip Right Heigh = " + HipYr + " Hip Left Height = " + HipYl); //Simetria de la cadera 
                    Debug.Log("Knee Right Depth = " + KneeeZ + "  Knee Left Depth = " + lelkneeZ); //Simetria de la rodilla 
                    Debug.Log("Ankle Right Depth = " + AnkleZr + "  Ankle Left Depth = " + AnkleZl); //Simetria del tobillo 
                    Debug.Log("Hip Right Depth = " + HipZr + "  Hip Left Depth = " + HipZl); //Simetria de la cadera 

                    Debug.Log("Diference between Ankles: " + DifAnkle);
                    Debug.Log("Diference between Hips: " + DifHip);
                    Debug.Log("Diference between Knees: " + Difknee);

                    //File results
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between elbows " + Difknee);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between shoulders " + DifHip);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C: \Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between wrists " + DifAnkleZ);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between elbows " + DifKneeZ);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

                }
                // ::::::::::::::::::::   FLEXION  Hip :::::::::::::::::::: //
                Debug.Log("flex check");    
                if (flex = true)
                {
                    //Variables Hip 

                    float x11 = body.Joints[JointType.HipRight].Position.X;
                    float y11 = body.Joints[JointType.HipRight].Position.Y;
                    float z11 = body.Joints[JointType.HipRight].Position.Z;
                    float x22 = body.Joints[JointType.HipLeft].Position.X;
                    float y22 = body.Joints[JointType.HipLeft].Position.Y;
                    float z22 = body.Joints[JointType.HipLeft].Position.Z;
                    float x33 = body.Joints[JointType.SpineMid].Position.X;
                    float y33 = body.Joints[JointType.SpineMid].Position.Y;
                    float z33 = body.Joints[JointType.SpineMid].Position.Z;
                    float k11 = body.Joints[JointType.KneeLeft].Position.X;
                    float k22 = body.Joints[JointType.KneeLeft].Position.Y;
                    float k33 = body.Joints[JointType.KneeLeft].Position.Z;
                    float k1l = body.Joints[JointType.KneeRight].Position.X;
                    float k2l = body.Joints[JointType.KneeRight].Position.Y;
                    float k3l = body.Joints[JointType.KneeRight].Position.Z;
                    float u11 = body.Joints[JointType.SpineBase].Position.X;
                    float u22 = body.Joints[JointType.SpineBase].Position.Y;
                    float u33 = body.Joints[JointType.SpineBase].Position.Z;


                    //plane1  (SpineMid-HipRight) 
                    Vector3 vector1 = new Vector3(x22 - x11, y22 - y11, z22 - z11); // Vectorial Result HipRight 
                    Vector3 vector2 = new Vector3(x33 - x11, y33 - y11, z33 - z11);
                    Vector3 Cp1;
                    Cp1 = Vector3.Cross(vector1, vector2); //Producto entre el vector 1 y 2 
                    Vector3 normalvect = new Vector3(Cp1.x, Cp1.y, Cp1.z); //Vector normal 
                    Vector3 Vec_Aux1 = new Vector3(x33 - u11, y33 - u22, z33 - u33);
                    Vector3 newcp;
                    newcp = Vector3.Cross(normalvect, Vec_Aux1);  //Vector resultante = vector normal, vector entre los dos puntos 
                    float daux1 = (newcp.x * (-x33) + newcp.y * (-y33) + newcp.z * (-z33)); //Magnitud del vector resultante
               
                   
                    //Right side projection and result Knee and Hip
                    float lambda_2 = (((-newcp.x) * k1l + (-newcp.y) * k2l + (-newcp.z) * k3l) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 KneeProject = new Vector3(k1l + (lambda_2 * newcp.x), k2l + (lambda_2 * newcp.y), k3l + (lambda_2 * newcp.z));
                    float KneeXr = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                    float KneeYr = body.Joints[JointType.ShoulderRight].Position.Y;
                    float KneeZr = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambda_aux1 = (((-newcp.x) * KneeXr + (-newcp.y) * KneeYr + (-newcp.z) * KneeZr) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Hipproject = new Vector3(KneeXr + (lambda_aux1 * newcp.x), KneeYr + (lambda_aux1 * newcp.y), KneeZr + (lambda_aux1 * newcp.z));
                    Vector3 VectorKnee1 = new Vector3(Hipproject.x - KneeProject.x, Hipproject.y - KneeProject.y, Hipproject.z - KneeProject.z);
                    float AngleFlex1 = Vector3.Angle(Vec_Aux1, VectorKnee1);
                    float AngleFlex1co = AngleFlex1; //Angulo de flexión

                    //left side Knee and Hip 
                    float lambda_3 = (((-newcp.x) * k11 + (-newcp.y) * k22 + (-newcp.z) * k33) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Kneeproject = new Vector3(k11 + (lambda_3 * newcp.x), k22 + (lambda_3 * newcp.y), k33 + (lambda_3 * newcp.z));
                    float KneeXl = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                    float KneeYl = body.Joints[JointType.ShoulderRight].Position.Y;
                    float KneeZl = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambda_4 = (((-newcp.x) * KneeXl + (-newcp.y) * KneeYl + (-newcp.z) * KneeZl) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Hipproj = new Vector3(KneeXl + (lambda_4 * newcp.x), KneeYl + (lambda_4 * newcp.y), KneeZl + (lambda_4 * newcp.z));
                    Vector3 Hip_A = new Vector3(Kneeproject.x - Hipproj.x, Kneeproject.y - Hipproj.y, Kneeproject.z - Hipproj.z);
                    float AngleFlexl = Vector3.Angle(Vec_Aux1, Hip_A);
                    float AngleFlexlcorrec = 180 - AngleFlexl; // Se le resta a 180 los grados que indique la operación
                    //StartCoroutine(Temporizador(AngleFlex1co, AngleFlexlcorrec));


                    ///////////////////////// Knee ///////////////////////////////////

                    float HipXr = body.Joints[JointType.HipRight].Position.X;
                    float HipYr = body.Joints[JointType.HipRight].Position.Y;
                    float HipZr = body.Joints[JointType.HipRight].Position.Z;
                    float KneeeX = body.Joints[JointType.KneeRight].Position.X;
                    float KneeeY = body.Joints[JointType.KneeRight].Position.Y;
                    float KneeeZ = body.Joints[JointType.KneeRight].Position.Z;
                    float AnkleXr = body.Joints[JointType.AnkleRight].Position.X;
                    float AnkleYr = body.Joints[JointType.AnkleRight].Position.Y;
                    float AnkleZr = body.Joints[JointType.AnkleRight].Position.Z;
                    float HipXl = body.Joints[JointType.HipLeft].Position.X;
                    float HipYl = body.Joints[JointType.HipLeft].Position.Y;
                    float HipZl = body.Joints[JointType.HipLeft].Position.Z;
                    float lelKneeX = body.Joints[JointType.KneeLeft].Position.X;
                    float lelKneeY = body.Joints[JointType.KneeLeft].Position.Y;
                    float lelKneeZ = body.Joints[JointType.KneeLeft].Position.Z;
                    float AnkleXl = body.Joints[JointType.AnkleLeft].Position.X;
                    float AnkleYl = body.Joints[JointType.AnkleLeft].Position.Y;
                    float AnkleZl = body.Joints[JointType.AnkleLeft].Position.Z;

                    //right side results Knee
                    Vector3 VecHKR = new Vector3(HipXr - KneeeX, HipYr - KneeeY, HipZr - KneeeZ); // Vector cadera- rodilla 
                    Vector3 VecKWR = new Vector3(AnkleXr - KneeeX, AnkleYr - KneeeY, AnkleZr - KneeeZ); // Vector rodilla- tobillo 
                    float AngleKneeR = Vector3.Angle(VecHKR, VecKWR); //Vector resultante 
                    float AngleKneeR_Co = 180 - AngleKneeR;
                    //Debug.Log(" Angle right Knee: " + AngleKneeR_Co); // Impresión del vector-- lo muestra angulo flexión de la rodilla 

                    //left side results Knee
                    Vector3 VecHKL = new Vector3(HipXl - lelKneeX, HipYl - lelKneeY, HipZl - lelKneeZ);
                    Vector3 VecHWL = new Vector3(AnkleXl - lelKneeX, AnkleYl - lelKneeY, AnkleZl - lelKneeZ);
                    float AngleKneeL = Vector3.Angle(VecHKL, VecHWL);
                    float AngleKneeL_Co = 180 - AngleKneeL;
                    //Debug.Log(" Angle left Knee: " + AngleKneeL_Co);
                    //StartCoroutine(Temporizador1(AngleKneeR_Co,AngleKneeL_Co));dg
                    Debug.Log("Temporizador");
                    StartCoroutine(Temporizador(AngleFlex1co, AngleFlexlcorrec, AngleKneeR_Co, AngleKneeL_Co));

                }

            }
        }

    }


    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.SetVertexCount(2);
            lr.material = BoneMaterial;
            lr.SetWidth(0.0f, 0.0f);

            jointObj.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }

        return body;


    }

    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;

            if (_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            Transform jointObj = bodyObject.transform.Find(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if (targetJoint.HasValue)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }
        }
    }

    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
            case Kinect.TrackingState.Tracked:
                return Color.green;

            case Kinect.TrackingState.Inferred:
                return Color.red;

            default:
                return Color.black;
        }
    }

    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}


/*using UnityEngine;
using System.Collections;
using Windows.Data;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using Windows.Kinect;
using System.Linq;
using System.IO;
using System;
using UnityEngine.UI;


public class BodySourceView : MonoBehaviour
{
    public Material BoneMaterial;
    public GameObject BodySourceManager;
    public GameObject _personaje1_Unity;
    public GameObject Master;
    public GameObject Reference;
    public GameObject Hips;
    public GameObject LeftUpLeg;
    public GameObject LeftLeg;
    public GameObject RightUpLeg;
    public GameObject RightLeg;
    public GameObject Spine;
    public GameObject Spine1;
    public GameObject Spine2;
    public GameObject LeftShoulder;
    public GameObject LeftArm;
    public GameObject LeftForeArm;
    public GameObject LeftHand;
    public GameObject Neck;
    public GameObject Head;
    public GameObject RightShoulder;
    public GameObject RightArm;
    public GameObject RightForeArm;
    public GameObject RightHand;
    public GameObject LeftFoot; //
    public GameObject RightFoot; //
    public GameObject RightAnkle; //
    public GameObject LeftAnkle; //
    public float F1 = 0;
    public float F2 = 0;
    public float F3 = 0;
    public float F4 = 0;
    public Text derecha = null; //Angulo Actual Derecho
    public Text izquierda = null; //Angulo Actual Izquierdo 
    public Text derecha_1 = null;
    public Text izquierda_1 = null;
    public float maxtot = 0;
    public int cont = -1;
    public bool rep = true;
    public Text angulo = null; //Angulo_Max_Derecho
    public Text angulo2 = null; //Angulo_Max_Izquierdo
    public Text angulo3 = null; //Angulo_Max_Derecho_Rodilla
    public Text angulo4 = null; //Angulo_Max_Izquierdo_Rodilla
    public bool angulonum = false;
    public GameObject semaforo;
    public GameObject semaforo2;
    public Text copiaej = null;
    public float valor;
    public float valor2;
    public float valor3;
    public float valor4;
    public Text repeticionesnum;
    public bool repeticiones0 = false;
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;


    // Canvax Maximos y minimos value
    /// </summary>

    public bool flex = false;
    public bool rodilla = false;
    public void ButtonClick()
    {
        flex = true;
        rodilla = true;
        angulonum = true;
        StartCoroutine(Seemaforo());
    }

    /// </summary>

    // Mapeo "puntos" del Cuerpo Humano

    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    public void Start()
    {

        _personaje1_Unity = GameObject.Find("personaje1_Unity");
        Master = GameObject.Find("master");
        Reference = GameObject.Find("Reference");
        Hips = GameObject.Find("Hips");
        LeftUpLeg = GameObject.Find("LeftUpLeg");
        LeftLeg = GameObject.Find("LeftLeg");
        RightUpLeg = GameObject.Find("RightUpLeg");
        RightLeg = GameObject.Find("RightLeg");
        Spine = GameObject.Find("Spine");
        Spine1 = GameObject.Find("Spine1");
        Spine2 = GameObject.Find("Spine2");
        LeftShoulder = GameObject.Find("LeftShoulder");
        LeftArm = GameObject.Find("LeftArm");
        LeftForeArm = GameObject.Find("LeftForeArm");
        LeftHand = GameObject.Find("LeftHand");
        Neck = GameObject.Find("Neck");
        Head = GameObject.Find("Head");
        RightShoulder = GameObject.Find("RightShoulder");
        RightArm = GameObject.Find("RightArm");
        RightForeArm = GameObject.Find("RightForeArm");
        RightHand = GameObject.Find("RightHand");
        RightAnkle = GameObject.Find("RightAnkle"); //
        LeftAnkle = GameObject.Find("LeftAnkle");   //
        RightFoot = GameObject.Find("RightFoot");  //
        LeftFoot = GameObject.Find("LeftFoot");    //

    }
    IEnumerator Seemaforo()
    {
        copiaej.text = "Realice el ejercicio propuesto";
        yield return new WaitForSeconds(5);
        Destroy(semaforo);
        copiaej.text = "Ahora es su turno!";
        semaforo2.SetActive(true);
    }

    IEnumerator Temporizador(float AngleFlex1co, float AngleFlexlcorrec, float AngleKneeR_Co, float AngleKneeL_Co)
    {
        flex = false;
        rodilla = false;
        yield return new WaitForSeconds(0.5f);// Mira angulos FLex Cada segundo miembro inferior
        MAX(AngleFlex1co, AngleFlexlcorrec, AngleKneeR_Co, AngleKneeL_Co);
        flex = true;
        rodilla = true; 

    }

    public void MAX(float AngleFlex1co, float AngleFlexlcorrec, float AngleKneeR_Co , float AngleKneeL_Co)
    {

        if (AngleFlex1co > F1)
        {
            F1 = AngleFlex1co;
        }
        if (AngleFlexlcorrec > F2)
        {
            F2 = AngleFlexlcorrec;
        }
        if (AngleKneeR_Co > F3)
        {
            F3 = AngleKneeR_Co;
        }
        if (AngleKneeL_Co > F4)
        {
            F4 = AngleKneeR_Co;
        }
        valor = AngleFlex1co;
        valor2 = AngleFlexlcorrec;
        valor3 = AngleKneeR_Co;
        valor4 = AngleKneeL_Co;
        derecha.text = ("" + AngleFlex1co);
        izquierda.text = ("" + AngleFlexlcorrec);
        derecha_1.text = ("" + AngleKneeR_Co);
        izquierda_1.text = ("" + AngleKneeL_Co);
        Repeticiones(valor);
        angulo.text = ("" + F1);//
        angulo2.text = ("" + F2);
        angulo3.text = ("" + F3);
        angulo4.text = ("" + F4);
    }

    public void Repeticiones(float valor)
    {
        repeticionesnum.text = ("" + cont);
        if (valor < 20 && rep == true)
        {
            if (repeticiones0 == true)
            {
                cont = cont + 1;
            }
            rep = false;
            Debug.Log(cont);

            if (cont == 10)
            {
                Debug.Log("Valor maximo enviado :   " + F1);
            }

        }
        if (valor > 20)
        {
            rep = true;
            repeticiones0 = true;
        }
    }
    void Update()
    {

        if (BodySourceManager == null)
        {
            return;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }

        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }

        var floorPlane = _BodyManager.FloorPlanes;
        var newrotation = Quaternion.FromToRotation(new Vector3(floorPlane.X, floorPlane.Y, floorPlane.Z), Vector3.up);


        List<ulong> trackedIds = new List<ulong>();
        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                trackedIds.Add(body.TrackingId);
            }
        }

        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);

        // First delete untracked bodies
        foreach (ulong trackingId in knownIds)
        {
            if (!trackedIds.Contains(trackingId))
            {

                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }

        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                if (!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }

            RefreshBodyObject(body, _Bodies[body.TrackingId]);

            Quaternion SpineBase = body.JointOrientations[JointType.SpineBase].Orientation.ChangeQuat(newrotation);
            Quaternion SpineMid = body.JointOrientations[JointType.SpineMid].Orientation.ChangeQuat(newrotation);
            Quaternion SpineShoulder = body.JointOrientations[JointType.SpineShoulder].Orientation.ChangeQuat(newrotation);
            Quaternion ShoulderLeft = body.JointOrientations[JointType.ShoulderLeft].Orientation.ChangeQuat(newrotation);
            Quaternion ShoulderRight = body.JointOrientations[JointType.ShoulderRight].Orientation.ChangeQuat(newrotation);
            Quaternion ElbowLeft = body.JointOrientations[JointType.ElbowLeft].Orientation.ChangeQuat(newrotation);
            Quaternion WristLeft = body.JointOrientations[JointType.WristLeft].Orientation.ChangeQuat(newrotation);
            Quaternion HandLeft = body.JointOrientations[JointType.HandLeft].Orientation.ChangeQuat(newrotation);
            Quaternion ElbowRight = body.JointOrientations[JointType.ElbowRight].Orientation.ChangeQuat(newrotation);
            Quaternion WristRight = body.JointOrientations[JointType.WristRight].Orientation.ChangeQuat(newrotation);
            Quaternion HandRight = body.JointOrientations[JointType.HandRight].Orientation.ChangeQuat(newrotation);
            Quaternion KneeLeft = body.JointOrientations[JointType.KneeLeft].Orientation.ChangeQuat(newrotation);
            Quaternion AnkleLeft = body.JointOrientations[JointType.AnkleLeft].Orientation.ChangeQuat(newrotation);
            Quaternion KneeRight = body.JointOrientations[JointType.KneeRight].Orientation.ChangeQuat(newrotation);
            Quaternion AnkleRight = body.JointOrientations[JointType.AnkleRight].Orientation.ChangeQuat(newrotation);
            Quaternion FootRight = body.JointOrientations[JointType.FootRight].Orientation.ChangeQuat(newrotation); //
            Quaternion FootLeft = body.JointOrientations[JointType.FootLeft].Orientation.ChangeQuat(newrotation); //
            FootRight.y = 1;
            FootRight.w = 0;
            FootLeft.y = 1;
            FootLeft.w = 0;
            Spine1.transform.rotation = SpineMid * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
            RightArm.transform.rotation = ElbowRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
            RightForeArm.transform.rotation = WristRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
            RightHand.transform.rotation = HandRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
            LeftArm.transform.rotation = ElbowLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
            LeftForeArm.transform.rotation = WristLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
            RightFoot.transform.rotation = FootRight * Quaternion.AngleAxis(0, new Vector3(0, 0, 1)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0)); // ojo -90
            LeftFoot.transform.rotation = FootLeft * Quaternion.AngleAxis(180, new Vector3(1, 0, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));//
            LeftHand.transform.rotation = HandLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
            RightUpLeg.transform.rotation = KneeRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
            RightLeg.transform.rotation = AnkleRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
            LeftUpLeg.transform.rotation = KneeLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
            LeftLeg.transform.rotation = AnkleLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

            var moveposition = body.Joints[JointType.SpineMid].Position;
            Master.transform.position = new Vector3(moveposition.X, moveposition.Y, -moveposition.Z);


            ////////////////////////////////////Miembro inferior////////////////////////////////////////////////////////////////////////////////////////////////////    
            // ::::::::::::::::::::::::::  HIP - KNEE EXERCISE HORIZONTAL  :::::::::::::::::::: // 

            if (Input.GetKeyDown(KeyCode.B))
            {
                Debug.Log("Horizontal Exercise selected");
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", " ::::::   HORIZONTAL EXERCISE   :::::: "); // Guardar los datos 
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

            }
            // ::::::::::::::::::::   SYMMETRY  BODY :::::::::::::::::::: //     

            if (Input.GetKeyDown(KeyCode.C))
            {
                //variables
                float KneeeX = body.Joints[JointType.KneeRight].Position.X;
                float KneeeY = body.Joints[JointType.KneeRight].Position.Y;
                float KneeeZ = body.Joints[JointType.KneeRight].Position.Z;
                float lelkneeX = body.Joints[JointType.KneeLeft].Position.X;
                float lelkneeY = body.Joints[JointType.KneeLeft].Position.Y;
                float lelkneeZ = body.Joints[JointType.KneeLeft].Position.Z;
                float AnkleXr = body.Joints[JointType.AnkleRight].Position.X;
                float AnkleYr = body.Joints[JointType.AnkleRight].Position.Y;
                float AnkleZr = body.Joints[JointType.AnkleRight].Position.Z;
                float AnkleXl = body.Joints[JointType.AnkleLeft].Position.X;
                float AnkleYl = body.Joints[JointType.AnkleLeft].Position.Y;
                float AnkleZl = body.Joints[JointType.AnkleLeft].Position.Z;
                float HipXr = body.Joints[JointType.HipRight].Position.X;
                float HipYr = body.Joints[JointType.HipRight].Position.Y;
                float HipZr = body.Joints[JointType.HipRight].Position.Z;
                float HipXl = body.Joints[JointType.HipLeft].Position.X;
                float HipYl = body.Joints[JointType.HipLeft].Position.Y;
                float HipZl = body.Joints[JointType.HipLeft].Position.Z;
                //results- Medicion de la simetria entre cada una de las partes 
                float Difknee = Mathf.Abs(KneeeY - lelkneeY) * 100; //Diferencia entre Rodilla izquierda y derecha 
                float DifAnkle = Mathf.Abs(AnkleYr - AnkleYl) * 100; //Diferencia entre Tobillo izquierdo y derecho 
                float DifHip = Math.Abs(HipYr - HipYl) * 100; //Diferencia cadera 
                float DifKneeZ = Mathf.Abs(KneeeZ - lelkneeZ) * 100; //Diferencia entre Rodilla derecha y izquierda 
                float DifAnkleZ = Mathf.Abs(AnkleZr - AnkleZl) * 100; //Diferencia entre entre derecha y izquierda 


                Debug.Log("Knee Right Heigh = " + KneeeY + "  Knee Left Heigh = " + lelkneeY); //Simetria de la rodilla 
                Debug.Log("Ankle Right Heigh = " + AnkleYr + "  Ankle Left Heigh = " + AnkleYl); //Simetria del tobillo
                Debug.Log("Hip Right Heigh = " + HipYr + " Hip Left Height = " + HipYl); //Simetria de la cadera 
                Debug.Log("Knee Right Depth = " + KneeeZ + "  Knee Left Depth = " + lelkneeZ); //Simetria de la rodilla 
                Debug.Log("Ankle Right Depth = " + AnkleZr + "  Ankle Left Depth = " + AnkleZl); //Simetria del tobillo 
                Debug.Log("Hip Right Depth = " + HipZr + "  Hip Left Depth = " + HipZl); //Simetria de la cadera 

                Debug.Log("Diference between Ankles: " + DifAnkle);
                Debug.Log("Diference between Hips: " + DifHip);
                Debug.Log("Diference between Knees: " + Difknee);

                //File results
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between elbows " + Difknee);
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between shoulders " + DifHip);
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                File.AppendAllText(@"C: \Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between wrists " + DifAnkleZ);
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between elbows " + DifKneeZ);
                File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

            }
            // ::::::::::::::::::::   FLEXION  Hip :::::::::::::::::::: //

            if (flex = true)
            {
                //Variables Hip 

                float x11 = body.Joints[JointType.HipRight].Position.X;
                float y11 = body.Joints[JointType.HipRight].Position.Y;
                float z11 = body.Joints[JointType.HipRight].Position.Z;
                float x22 = body.Joints[JointType.HipLeft].Position.X;
                float y22 = body.Joints[JointType.HipLeft].Position.Y;
                float z22 = body.Joints[JointType.HipLeft].Position.Z;
                float x33 = body.Joints[JointType.SpineMid].Position.X;
                float y33 = body.Joints[JointType.SpineMid].Position.Y;
                float z33 = body.Joints[JointType.SpineMid].Position.Z;
                float k11 = body.Joints[JointType.KneeLeft].Position.X;
                float k22 = body.Joints[JointType.KneeLeft].Position.Y;
                float k33 = body.Joints[JointType.KneeLeft].Position.Z;
                float k1l = body.Joints[JointType.KneeRight].Position.X;
                float k2l = body.Joints[JointType.KneeRight].Position.Y;
                float k3l = body.Joints[JointType.KneeRight].Position.Z;
                float u11 = body.Joints[JointType.SpineBase].Position.X;
                float u22 = body.Joints[JointType.SpineBase].Position.Y;
                float u33 = body.Joints[JointType.SpineBase].Position.Z;


                //plane1  (SpineMid-HipRight) 
                Vector3 vector1 = new Vector3(x22 - x11, y22 - y11, z22 - z11); // Vectorial Result HipRight 
                Vector3 vector2 = new Vector3(x33 - x11, y33 - y11, z33 - z11);
                Vector3 Cp1;
                Cp1 = Vector3.Cross(vector1, vector2); //Producto entre el vector 1 y 2 
                Vector3 normalvect = new Vector3(Cp1.x, Cp1.y, Cp1.z); //Vector normal 
                Vector3 Vec_Aux1 = new Vector3(x33 - u11, y33 - u22, z33 - u33);
                Vector3 newcp;
                newcp = Vector3.Cross(normalvect, Vec_Aux1);  //Vector resultante = vector normal, vector entre los dos puntos 
                float daux1 = (newcp.x * (-x33) + newcp.y * (-y33) + newcp.z * (-z33)); //Magnitud del vector resultante

                //Right side projection and result Knee and Hip
                float lambda_2 = (((-newcp.x) * k1l + (-newcp.y) * k2l + (-newcp.z) * k3l) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                Vector3 KneeProject = new Vector3(k1l + (lambda_2 * newcp.x), k2l + (lambda_2 * newcp.y), k3l + (lambda_2 * newcp.z));
                float KneeXr = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                float KneeYr = body.Joints[JointType.ShoulderRight].Position.Y;
                float KneeZr = body.Joints[JointType.ShoulderRight].Position.Z;
                float lambda_aux1 = (((-newcp.x) * KneeXr + (-newcp.y) * KneeYr + (-newcp.z) * KneeZr) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                Vector3 Hipproject = new Vector3(KneeXr + (lambda_aux1 * newcp.x), KneeYr + (lambda_aux1 * newcp.y), KneeZr + (lambda_aux1 * newcp.z));
                Vector3 VectorKnee1 = new Vector3(Hipproject.x - KneeProject.x, Hipproject.y - KneeProject.y, Hipproject.z - KneeProject.z);
                float AngleFlex1 = Vector3.Angle(Vec_Aux1, VectorKnee1);
                float AngleFlex1co = 180 - AngleFlex1; //Angulo de flexión

                //left side Knee and Hip 
                float lambda_3 = (((-newcp.x) * k11 + (-newcp.y) * k22 + (-newcp.z) * k33) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                Vector3 Kneeproject = new Vector3(k11 + (lambda_3 * newcp.x), k22 + (lambda_3 * newcp.y), k33 + (lambda_3 * newcp.z));
                float KneeXl = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                float KneeYl = body.Joints[JointType.ShoulderRight].Position.Y;
                float KneeZl = body.Joints[JointType.ShoulderRight].Position.Z;
                float lambda_4 = (((-newcp.x) * KneeXl + (-newcp.y) * KneeYl + (-newcp.z) * KneeZl) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                Vector3 Hipproj = new Vector3(KneeXl + (lambda_4 * newcp.x), KneeYl + (lambda_4 * newcp.y), KneeZl + (lambda_4 * newcp.z));
                Vector3 Hip_A = new Vector3(Kneeproject.x - Hipproj.x, Kneeproject.y - Hipproj.y, Kneeproject.z - Hipproj.z);
                float AngleFlexl = Vector3.Angle(Vec_Aux1, Hip_A);
                float AngleFlexlcorrec = 180 - AngleFlexl; // Se le resta a 180 los grados que indique la operación
                //StartCoroutine(Temporizador(AngleFlex1co, AngleFlexlcorrec));

                ///////////////////////// Knee ///////////////////////////////////

                float HipXr = body.Joints[JointType.HipRight].Position.X;
                float HipYr = body.Joints[JointType.HipRight].Position.Y;
                float HipZr = body.Joints[JointType.HipRight].Position.Z;
                float KneeeX = body.Joints[JointType.KneeRight].Position.X;
                float KneeeY = body.Joints[JointType.KneeRight].Position.Y;
                float KneeeZ = body.Joints[JointType.KneeRight].Position.Z;
                float AnkleXr = body.Joints[JointType.AnkleRight].Position.X;
                float AnkleYr = body.Joints[JointType.AnkleRight].Position.Y;
                float AnkleZr = body.Joints[JointType.AnkleRight].Position.Z;
                float HipXl = body.Joints[JointType.HipLeft].Position.X;
                float HipYl = body.Joints[JointType.HipLeft].Position.Y;
                float HipZl = body.Joints[JointType.HipLeft].Position.Z;
                float lelKneeX = body.Joints[JointType.KneeLeft].Position.X;
                float lelKneeY = body.Joints[JointType.KneeLeft].Position.Y;
                float lelKneeZ = body.Joints[JointType.KneeLeft].Position.Z;
                float AnkleXl = body.Joints[JointType.AnkleLeft].Position.X;
                float AnkleYl = body.Joints[JointType.AnkleLeft].Position.Y;
                float AnkleZl = body.Joints[JointType.AnkleLeft].Position.Z;

                //right side results Knee
                Vector3 VecHKR = new Vector3(HipXr - KneeeX, HipYr - KneeeY, HipZr - KneeeZ); // Vector cadera- rodilla 
                Vector3 VecKWR = new Vector3(AnkleXr - KneeeX, AnkleYr - KneeeY, AnkleZr - KneeeZ); // Vector rodilla- tobillo 
                float AngleKneeR = Vector3.Angle(VecHKR, VecKWR); //Vector resultante 
                float AngleKneeR_Co = 180 - AngleKneeR;
                //Debug.Log(" Angle right Knee: " + AngleKneeR_Co); // Impresión del vector-- lo muestra angulo flexión de la rodilla 

                //left side results Knee
                Vector3 VecHKL = new Vector3(HipXl - lelKneeX, HipYl - lelKneeY, HipZl - lelKneeZ);
                Vector3 VecHWL = new Vector3(AnkleXl - lelKneeX, AnkleYl - lelKneeY, AnkleZl - lelKneeZ);
                float AngleKneeL = Vector3.Angle(VecHKL, VecHWL);
                float AngleKneeL_Co = 180 - AngleKneeL;
                //Debug.Log(" Angle left Knee: " + AngleKneeL_Co);
                //StartCoroutine(Temporizador1(AngleKneeR_Co,AngleKneeL_Co));
                StartCoroutine(Temporizador(AngleFlex1co, AngleFlexlcorrec, AngleKneeR_Co, AngleKneeL_Co));

                }

            }
        }

    }

    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.SetVertexCount(2);
            lr.material = BoneMaterial;
            lr.SetWidth(0.0f, 0.0f);

            jointObj.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }

        return body;


    }

    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;

            if (_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            Transform jointObj = bodyObject.transform.Find(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if (targetJoint.HasValue)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }
        }
    }

    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
            case Kinect.TrackingState.Tracked:
                return Color.green;

            case Kinect.TrackingState.Inferred:
                return Color.red;

            default:
                return Color.black;
        }
    }

    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}
/*
using UnityEngine;
using System.Collections;
using Windows.Data;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using Windows.Kinect;
using System.Linq;
using System.IO;
using System;
using UnityEngine.UI;


public class BodySourceView : MonoBehaviour
{
    public Material BoneMaterial;
    public GameObject BodySourceManager;
    public GameObject _personaje1_Unity;
    public GameObject Master;
    public GameObject Reference;
    public GameObject Hips;
    public GameObject LeftUpLeg;
    public GameObject LeftLeg;
    public GameObject RightUpLeg;
    public GameObject RightLeg;
    public GameObject Spine;
    public GameObject Spine1;
    public GameObject Spine2;
    public GameObject LeftShoulder;
    public GameObject LeftArm;
    public GameObject LeftForeArm;
    public GameObject LeftHand;
    public GameObject Neck;
    public GameObject Head;
    public GameObject RightShoulder;
    public GameObject RightArm;
    public GameObject RightForeArm;
    public GameObject RightHand;
    public GameObject LeftFoot; //
    public GameObject RightFoot; //
    public GameObject RightAnkle; //
    public GameObject LeftAnkle; //
    public float F1 = 0;
    public float F2 = 0;
    public float F3 = 0;
    public float F4 = 0;
    public Text derecha = null; //Angulo Actual Derecho
    public Text izquierda = null; //Angulo Actual Izquierdo 
    public Text derecha_1 = null;
    public Text izquierda_1 = null; 
    public float maxtot = 0;
    public int cont = -1;
    public int cont1 = -1;
    public bool rep = true;
    public bool rep1 = true;
    public Text angulo = null; //Angulo_Max_Derecho
    public Text angulo2 = null; //Angulo_Max_Izquierdo
    public Text angulo3 = null; //Angulo_Max_Derecho_Rodilla
    public Text angulo4 = null; //Angulo_Max_Derecho_Rodilla
    public bool angulonum = false;
    public GameObject semaforo;
    public GameObject semaforo2;
    public Text copiaej = null;
    public float valor;
    public float valor2;
    public float valor3;
    public float valor4;
    public Text repeticionesnum;
    public bool repeticiones0 = false;
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;


// Canvax Maximos y minimos value
/// </summary>

    public bool flex = false;
    public void ButtonClick()
    {
        flex = true;
        angulonum = true; 
        StartCoroutine(Seemaforo());
   }

/// </summary>

// Mapeo "puntos" del Cuerpo Humano

private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    public void Start()
    {
        
        _personaje1_Unity = GameObject.Find("personaje1_Unity");
        Master = GameObject.Find("master");
        Reference = GameObject.Find("Reference");
        Hips = GameObject.Find("Hips");
        LeftUpLeg = GameObject.Find("LeftUpLeg");
        LeftLeg = GameObject.Find("LeftLeg");
        RightUpLeg = GameObject.Find("RightUpLeg");
        RightLeg = GameObject.Find("RightLeg");
        Spine = GameObject.Find("Spine");
        Spine1 = GameObject.Find("Spine1");
        Spine2 = GameObject.Find("Spine2");
        LeftShoulder = GameObject.Find("LeftShoulder");
        LeftArm = GameObject.Find("LeftArm");
        LeftForeArm = GameObject.Find("LeftForeArm");
        LeftHand = GameObject.Find("LeftHand");
        Neck = GameObject.Find("Neck");
        Head = GameObject.Find("Head");
        RightShoulder = GameObject.Find("RightShoulder");
        RightArm = GameObject.Find("RightArm");
        RightForeArm = GameObject.Find("RightForeArm");
        RightHand = GameObject.Find("RightHand");
        RightAnkle = GameObject.Find("RightAnkle"); //
        LeftAnkle = GameObject.Find("LeftAnkle");   //
        RightFoot = GameObject.Find("RightFoot");  //
        LeftFoot = GameObject.Find("LeftFoot");    //
        
    }
    IEnumerator Seemaforo()
    {
        copiaej.text = "Realice el ejercicio propuesto";
        yield return new WaitForSeconds(5);
        Destroy(semaforo);
        copiaej.text = "Ahora es su turno!";
        semaforo2.SetActive(true);
    }
    
    IEnumerator Temporizador(float AngleFlex1co, float AngleFlexlcorrec)
    {
        flex = false;
        yield return new WaitForSeconds(0.5f);// Mira angulos FLex Cada segundo miembro inferior
        MAX(AngleFlex1co, AngleFlexlcorrec);
        flex = true;
        
    } 

    public void MAX(float AngleFlex1co, float AngleFlexlcorrec)
    {

        if (AngleFlex1co > F1)
        {
            F1 = AngleFlex1co;
        }
        if (AngleFlexlcorrec > F2)
        {
            F2 = AngleFlexlcorrec;
        }
        valor = AngleFlex1co;
        valor2 = AngleFlexlcorrec;
        derecha.text = ("" + AngleFlex1co);
        izquierda.text = ("" + AngleFlexlcorrec);
        Repeticiones(valor);
        angulo.text = ("" + F1);//
        angulo2.text = ("" + F2);
     }

    public void Repeticiones(float valor)
    {
        repeticionesnum.text = ("" + cont);
        if (valor < 20 && rep == true)
        {
            if (repeticiones0 == true)
            {
                cont = cont + 1;
            }
            rep = false;
            Debug.Log(cont);

            if (cont == 10)
            {
                Debug.Log("Valor maximo enviado :   " + F1);

            }

        }
        if (valor > 20)
        {
            rep = true;
            repeticiones0 = true;
        }
    }

    IEnumerator Temporizador1(float AngleKneeR_Co, float AngleKneeL_Co)
    {
        flex = false;
        yield return new WaitForSeconds(0.5f);// Mira angulos FLex Cada segundo miembro inferior
        MAX1(AngleKneeR_Co, AngleKneeL_Co);
        flex = true;
        
    } 
    public void MAX1(float AngleKneeR_Co, float AngleKneeL_Co)
    {

        if (AngleKneeR_Co > F3)
        {
            F3 = AngleKneeR_Co;
        }
        if (AngleKneeL_Co > F4)
        {
            F4 = AngleKneeL_Co;
        }
        valor3 = AngleKneeR_Co;
        valor4 = AngleKneeL_Co;
        derecha_1.text = ("" + AngleKneeR_Co);
        izquierda_1.text = ("" + AngleKneeL_Co);
        Repeticiones1(valor3);
        angulo3.text = ("" + F3);//
        angulo4.text = ("" + F4);
     }

    public void Repeticiones1(float valor3)
    {
        if (valor3 < 20 && rep1 == true)
        {
            cont1 = cont1 +1;
            rep1 = false;
            Debug.Log(cont1);

            if (cont1 == 10)
            {
                Debug.Log("Valor maximo enviado :   " + F3);
                
            }

        }
        if (valor3 >20)
        {
            rep1 = true;

        }

    }
    void Update()
    {

        if (BodySourceManager == null)
        {
            return;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }

        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }

        var floorPlane = _BodyManager.FloorPlanes;
        var newrotation = Quaternion.FromToRotation(new Vector3(floorPlane.X, floorPlane.Y, floorPlane.Z), Vector3.up);


        List<ulong> trackedIds = new List<ulong>();
        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                trackedIds.Add(body.TrackingId);
            }
        }

        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);

        // First delete untracked bodies
        foreach (ulong trackingId in knownIds)
        {
            if (!trackedIds.Contains(trackingId))
            {
                
                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }

        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                if (!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }

                RefreshBodyObject(body, _Bodies[body.TrackingId]);

                Quaternion SpineBase = body.JointOrientations[JointType.SpineBase].Orientation.ChangeQuat(newrotation);
                Quaternion SpineMid = body.JointOrientations[JointType.SpineMid].Orientation.ChangeQuat(newrotation);
                Quaternion SpineShoulder = body.JointOrientations[JointType.SpineShoulder].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderLeft = body.JointOrientations[JointType.ShoulderLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderRight = body.JointOrientations[JointType.ShoulderRight].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowLeft = body.JointOrientations[JointType.ElbowLeft].Orientation.ChangeQuat(newrotation);
                Quaternion WristLeft = body.JointOrientations[JointType.WristLeft].Orientation.ChangeQuat(newrotation);
                Quaternion HandLeft = body.JointOrientations[JointType.HandLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowRight = body.JointOrientations[JointType.ElbowRight].Orientation.ChangeQuat(newrotation);
                Quaternion WristRight = body.JointOrientations[JointType.WristRight].Orientation.ChangeQuat(newrotation);
                Quaternion HandRight = body.JointOrientations[JointType.HandRight].Orientation.ChangeQuat(newrotation);
                Quaternion KneeLeft = body.JointOrientations[JointType.KneeLeft].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleLeft = body.JointOrientations[JointType.AnkleLeft].Orientation.ChangeQuat(newrotation);
                Quaternion KneeRight = body.JointOrientations[JointType.KneeRight].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleRight = body.JointOrientations[JointType.AnkleRight].Orientation.ChangeQuat(newrotation);
                Quaternion FootRight = body.JointOrientations[JointType.FootRight].Orientation.ChangeQuat(newrotation); //
                Quaternion FootLeft = body.JointOrientations[JointType.FootLeft].Orientation.ChangeQuat(newrotation); //
                FootRight.y = 1;
                FootRight.w = 0;
                FootLeft.y = 1;
                FootLeft.w = 0;
                Spine1.transform.rotation = SpineMid * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightArm.transform.rotation = ElbowRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightForeArm.transform.rotation = WristRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightHand.transform.rotation = HandRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftArm.transform.rotation = ElbowLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftForeArm.transform.rotation = WristLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightFoot.transform.rotation = FootRight * Quaternion.AngleAxis(0, new Vector3(0, 0, 1)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0)); // ojo -90
                LeftFoot.transform.rotation = FootLeft * Quaternion.AngleAxis(180, new Vector3(1, 0, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));//
                LeftHand.transform.rotation = HandLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightUpLeg.transform.rotation = KneeRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightLeg.transform.rotation = AnkleRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftUpLeg.transform.rotation = KneeLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftLeg.transform.rotation = AnkleLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

                var moveposition = body.Joints[JointType.SpineMid].Position;
                Master.transform.position = new Vector3(moveposition.X, moveposition.Y, -moveposition.Z);


                ////////////////////////////////////Miembro inferior////////////////////////////////////////////////////////////////////////////////////////////////////    
                // ::::::::::::::::::::::::::  HIP - KNEE EXERCISE HORIZONTAL  :::::::::::::::::::: // 

                if (Input.GetKeyDown(KeyCode.B))
                {
                    Debug.Log("Horizontal Exercise selected");
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", " ::::::   HORIZONTAL EXERCISE   :::::: "); // Guardar los datos 
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

                }
                // ::::::::::::::::::::   SYMMETRY  BODY :::::::::::::::::::: //     

                if (Input.GetKeyDown(KeyCode.C))
                {
                    //variables
                    float KneeeX = body.Joints[JointType.KneeRight].Position.X;
                    float KneeeY = body.Joints[JointType.KneeRight].Position.Y;
                    float KneeeZ = body.Joints[JointType.KneeRight].Position.Z;
                    float lelkneeX = body.Joints[JointType.KneeLeft].Position.X;
                    float lelkneeY = body.Joints[JointType.KneeLeft].Position.Y;
                    float lelkneeZ = body.Joints[JointType.KneeLeft].Position.Z;
                    float AnkleXr = body.Joints[JointType.AnkleRight].Position.X;
                    float AnkleYr = body.Joints[JointType.AnkleRight].Position.Y;
                    float AnkleZr = body.Joints[JointType.AnkleRight].Position.Z;
                    float AnkleXl = body.Joints[JointType.AnkleLeft].Position.X;
                    float AnkleYl = body.Joints[JointType.AnkleLeft].Position.Y;
                    float AnkleZl = body.Joints[JointType.AnkleLeft].Position.Z;
                    float HipXr = body.Joints[JointType.HipRight].Position.X;
                    float HipYr = body.Joints[JointType.HipRight].Position.Y;
                    float HipZr = body.Joints[JointType.HipRight].Position.Z;
                    float HipXl = body.Joints[JointType.HipLeft].Position.X;
                    float HipYl = body.Joints[JointType.HipLeft].Position.Y;
                    float HipZl = body.Joints[JointType.HipLeft].Position.Z;
                    //results- Medicion de la simetria entre cada una de las partes 
                    float Difknee = Mathf.Abs(KneeeY - lelkneeY) * 100; //Diferencia entre Rodilla izquierda y derecha 
                    float DifAnkle = Mathf.Abs(AnkleYr - AnkleYl) * 100; //Diferencia entre Tobillo izquierdo y derecho 
                    float DifHip = Math.Abs(HipYr - HipYl) * 100; //Diferencia cadera 
                    float DifKneeZ = Mathf.Abs(KneeeZ - lelkneeZ) * 100; //Diferencia entre Rodilla derecha y izquierda 
                    float DifAnkleZ = Mathf.Abs(AnkleZr - AnkleZl) * 100; //Diferencia entre entre derecha y izquierda 


                    Debug.Log("Knee Right Heigh = " + KneeeY + "  Knee Left Heigh = " + lelkneeY); //Simetria de la rodilla 
                    Debug.Log("Ankle Right Heigh = " + AnkleYr + "  Ankle Left Heigh = " + AnkleYl); //Simetria del tobillo
                    Debug.Log("Hip Right Heigh = " + HipYr + " Hip Left Height = " + HipYl); //Simetria de la cadera 
                    Debug.Log("Knee Right Depth = " + KneeeZ + "  Knee Left Depth = " + lelkneeZ); //Simetria de la rodilla 
                    Debug.Log("Ankle Right Depth = " + AnkleZr + "  Ankle Left Depth = " + AnkleZl); //Simetria del tobillo 
                    Debug.Log("Hip Right Depth = " + HipZr + "  Hip Left Depth = " + HipZl); //Simetria de la cadera 

                    Debug.Log("Diference between Ankles: " + DifAnkle);
                    Debug.Log("Diference between Hips: " + DifHip);
                    Debug.Log("Diference between Knees: " + Difknee);

                    //File results
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between elbows " + Difknee);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between shoulders " + DifHip);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C: \Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between wrists " + DifAnkleZ);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between elbows " + DifKneeZ);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

                }
                // ::::::::::::::::::::   FLEXION  Hip :::::::::::::::::::: //

                if (flex = true)
                {
                    //Variables Hip 

                    float x11 = body.Joints[JointType.HipRight].Position.X;
                    float y11 = body.Joints[JointType.HipRight].Position.Y;
                    float z11 = body.Joints[JointType.HipRight].Position.Z;
                    float x22 = body.Joints[JointType.HipLeft].Position.X;
                    float y22 = body.Joints[JointType.HipLeft].Position.Y;
                    float z22 = body.Joints[JointType.HipLeft].Position.Z;
                    float x33 = body.Joints[JointType.SpineMid].Position.X;
                    float y33 = body.Joints[JointType.SpineMid].Position.Y;
                    float z33 = body.Joints[JointType.SpineMid].Position.Z;
                    float k11 = body.Joints[JointType.KneeLeft].Position.X;
                    float k22 = body.Joints[JointType.KneeLeft].Position.Y;
                    float k33 = body.Joints[JointType.KneeLeft].Position.Z;
                    float k1l = body.Joints[JointType.KneeRight].Position.X;
                    float k2l = body.Joints[JointType.KneeRight].Position.Y;
                    float k3l = body.Joints[JointType.KneeRight].Position.Z;
                    float u11 = body.Joints[JointType.SpineBase].Position.X;
                    float u22 = body.Joints[JointType.SpineBase].Position.Y;
                    float u33 = body.Joints[JointType.SpineBase].Position.Z;


                    //plane1  (SpineMid-HipRight) 
                    Vector3 vector1 = new Vector3(x22 - x11, y22 - y11, z22 - z11); // Vectorial Result HipRight 
                    Vector3 vector2 = new Vector3(x33 - x11, y33 - y11, z33 - z11);
                    Vector3 Cp1;
                    Cp1 = Vector3.Cross(vector1, vector2); //Producto entre el vector 1 y 2 
                    Vector3 normalvect = new Vector3(Cp1.x, Cp1.y, Cp1.z); //Vector normal 
                    Vector3 Vec_Aux1 = new Vector3(x33 - u11, y33 - u22, z33 - u33);
                    Vector3 newcp;
                    newcp = Vector3.Cross(normalvect, Vec_Aux1);  //Vector resultante = vector normal, vector entre los dos puntos 
                    float daux1 = (newcp.x * (-x33) + newcp.y * (-y33) + newcp.z * (-z33)); //Magnitud del vector resultante


                    //Right side projection and result Knee and Hip
                    float lambda_2 = (((-newcp.x) * k1l + (-newcp.y) * k2l + (-newcp.z) * k3l) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 KneeProject = new Vector3(k1l + (lambda_2 * newcp.x), k2l + (lambda_2 * newcp.y), k3l + (lambda_2 * newcp.z));
                    float KneeXr = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                    float KneeYr = body.Joints[JointType.ShoulderRight].Position.Y;
                    float KneeZr = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambda_aux1 = (((-newcp.x) * KneeXr + (-newcp.y) * KneeYr + (-newcp.z) * KneeZr) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Hipproject = new Vector3(KneeXr + (lambda_aux1 * newcp.x), KneeYr + (lambda_aux1 * newcp.y), KneeZr + (lambda_aux1 * newcp.z));
                    Vector3 VectorKnee1 = new Vector3(Hipproject.x - KneeProject.x, Hipproject.y - KneeProject.y, Hipproject.z - KneeProject.z);
                    float AngleFlex1 = Vector3.Angle(Vec_Aux1, VectorKnee1);
                    float AngleFlex1co = 180 - AngleFlex1; //Angulo de flexión

                    //left side Knee and Hip 
                    float lambda_3 = (((-newcp.x) * k11 + (-newcp.y) * k22 + (-newcp.z) * k33) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Kneeproject = new Vector3(k11 + (lambda_3 * newcp.x), k22 + (lambda_3 * newcp.y), k33 + (lambda_3 * newcp.z));
                    float KneeXl = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                    float KneeYl = body.Joints[JointType.ShoulderRight].Position.Y;
                    float KneeZl = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambda_4 = (((-newcp.x) * KneeXl + (-newcp.y) * KneeYl + (-newcp.z) * KneeZl) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Hipproj = new Vector3(KneeXl + (lambda_4 * newcp.x), KneeYl + (lambda_4 * newcp.y), KneeZl + (lambda_4 * newcp.z));
                    Vector3 Hip_A = new Vector3(Kneeproject.x - Hipproj.x, Kneeproject.y - Hipproj.y, Kneeproject.z - Hipproj.z);
                    float AngleFlexl = Vector3.Angle(Vec_Aux1, Hip_A);
                    float AngleFlexlcorrec = 180 - AngleFlexl; // Se le resta a 180 los grados que indique la operación
                    StartCoroutine(Temporizador(AngleFlex1co, AngleFlexlcorrec));
                
                    ///////////////////////// Knee ///////////////////////////////////

                    float HipXr = body.Joints[JointType.HipRight].Position.X;
                    float HipYr = body.Joints[JointType.HipRight].Position.Y;
                    float HipZr = body.Joints[JointType.HipRight].Position.Z;
                    float KneeeX = body.Joints[JointType.KneeRight].Position.X;
                    float KneeeY = body.Joints[JointType.KneeRight].Position.Y;
                    float KneeeZ = body.Joints[JointType.KneeRight].Position.Z;
                    float AnkleXr = body.Joints[JointType.AnkleRight].Position.X;
                    float AnkleYr = body.Joints[JointType.AnkleRight].Position.Y;
                    float AnkleZr = body.Joints[JointType.AnkleRight].Position.Z;
                    float HipXl = body.Joints[JointType.HipLeft].Position.X;
                    float HipYl = body.Joints[JointType.HipLeft].Position.Y;
                    float HipZl = body.Joints[JointType.HipLeft].Position.Z;
                    float lelKneeX = body.Joints[JointType.KneeLeft].Position.X;
                    float lelKneeY = body.Joints[JointType.KneeLeft].Position.Y;
                    float lelKneeZ = body.Joints[JointType.KneeLeft].Position.Z;
                    float AnkleXl = body.Joints[JointType.AnkleLeft].Position.X;
                    float AnkleYl = body.Joints[JointType.AnkleLeft].Position.Y;
                    float AnkleZl = body.Joints[JointType.AnkleLeft].Position.Z;

                    //right side results Knee
                    Vector3 VecHKR = new Vector3(HipXr - KneeeX, HipYr - KneeeY, HipZr - KneeeZ); // Vector cadera- rodilla 
                    Vector3 VecKWR = new Vector3(AnkleXr - KneeeX, AnkleYr - KneeeY, AnkleZr - KneeeZ); // Vector rodilla- tobillo 
                    float AngleKneeR = Vector3.Angle(VecHKR, VecKWR); //Vector resultante 
                    float AngleKneeR_Co = 180 - AngleKneeR;
                    //Debug.Log(" Angle right Knee: " + AngleKneeR_Co); // Impresión del vector-- lo muestra angulo flexión de la rodilla 

                    //left side results Knee
                    Vector3 VecHKL = new Vector3(HipXl - lelKneeX, HipYl - lelKneeY, HipZl - lelKneeZ);
                    Vector3 VecHWL = new Vector3(AnkleXl - lelKneeX, AnkleYl - lelKneeY, AnkleZl - lelKneeZ);
                    float AngleKneeL = Vector3.Angle(VecHKL, VecHWL);
                    float AngleKneeL_Co = 180 - AngleKneeL;
                    //Debug.Log(" Angle left Knee: " + AngleKneeL_Co);
                    StartCoroutine(Temporizador1(AngleKneeR_Co,AngleKneeL_Co));
                }
                
            }
        }

    }


    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.SetVertexCount(2);
            lr.material = BoneMaterial;
            lr.SetWidth(0.0f, 0.0f);

            jointObj.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }

        return body;


    }

    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;

            if (_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            Transform jointObj = bodyObject.transform.Find(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if (targetJoint.HasValue)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }
        }
    }

    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
            case Kinect.TrackingState.Tracked:
                return Color.green;

            case Kinect.TrackingState.Inferred:
                return Color.red;

            default:
                return Color.black;
        }
    }

    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}


/*
using UnityEngine;
using System.Collections;
using Windows.Data;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using Windows.Kinect;
using System.Linq;
using System.IO;
using System;
using UnityEngine.UI;


public class BodySourceView : MonoBehaviour
{
    public Material BoneMaterial;
    public GameObject BodySourceManager;
    public GameObject _personaje1_Unity;
    public GameObject Master;
    public GameObject Reference;
    public GameObject Hips;
    public GameObject LeftUpLeg;
    public GameObject LeftLeg;
    public GameObject RightUpLeg;
    public GameObject RightLeg;
    public GameObject Spine;
    public GameObject Spine1;
    public GameObject Spine2;
    public GameObject LeftShoulder;
    public GameObject LeftArm;
    public GameObject LeftForeArm;
    public GameObject LeftHand;
    public GameObject Neck;
    public GameObject Head;
    public GameObject RightShoulder;
    public GameObject RightArm;
    public GameObject RightForeArm;
    public GameObject RightHand;
    public GameObject LeftFoot; //
    public GameObject RightFoot; //
    public GameObject RightAnkle; //
    public GameObject LeftAnkle; //
    public float F1 = 0;
    public float F2 = 0;
    public Text derecha = null; //Angulo Actual Derecho
    public Text izquierda = null; //Angulo Actual Izquierdo 
    public float maxtot = 0;
    public int cont = -1;
    public bool rep = true;
    public Text angulo = null; //Angulo_Max_Derecho
    public Text angulo2 = null; //Angulo_Max_Izquierdo
    public bool angulonum = false;
    public GameObject semaforo;
    public GameObject semaforo2;
    public Text copiaej = null;
    public float valor;
    public float valor2;
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;
    public Text repeticionesnum;
    public bool repeticiones0 = false;

    // Canvax Maximos y minimos value
    /// </summary>

    public bool flex = false;
    public void Flex()
    {
        flex = true;
        StartCoroutine(Seemaforo());
   }

/// </summary>

// Mapeo "puntos" del Cuerpo Humano

private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    public void Start()
    {
        
        _personaje1_Unity = GameObject.Find("personaje1_Unity");
        Master = GameObject.Find("master");
        Reference = GameObject.Find("Reference");
        Hips = GameObject.Find("Hips");
        LeftUpLeg = GameObject.Find("LeftUpLeg");
        LeftLeg = GameObject.Find("LeftLeg");
        RightUpLeg = GameObject.Find("RightUpLeg");
        RightLeg = GameObject.Find("RightLeg");
        Spine = GameObject.Find("Spine");
        Spine1 = GameObject.Find("Spine1");
        Spine2 = GameObject.Find("Spine2");
        LeftShoulder = GameObject.Find("LeftShoulder");
        LeftArm = GameObject.Find("LeftArm");
        LeftForeArm = GameObject.Find("LeftForeArm");
        LeftHand = GameObject.Find("LeftHand");
        Neck = GameObject.Find("Neck");
        Head = GameObject.Find("Head");
        RightShoulder = GameObject.Find("RightShoulder");
        RightArm = GameObject.Find("RightArm");
        RightForeArm = GameObject.Find("RightForeArm");
        RightHand = GameObject.Find("RightHand");
        RightAnkle = GameObject.Find("RightAnkle"); //
        LeftAnkle = GameObject.Find("LeftAnkle");   //
        RightFoot = GameObject.Find("RightFoot");  //
        LeftFoot = GameObject.Find("LeftFoot");    //
        
    }
    IEnumerator Seemaforo()
    {
        copiaej.text = "Realice el ejercicio propuesto";
        yield return new WaitForSeconds(5);
        Destroy(semaforo);
        copiaej.text = "Ahora es su turno!";
        semaforo2.SetActive(true);
    }
    
    IEnumerator Temporizador(float AngleFlex1co, float AngleFlexlcorrec)
    {
        flex = false;
        yield return new WaitForSeconds(0.5f);// Mira angulos FLex Cada segundo miembro inferior
        MAX(AngleFlex1co, AngleFlexlcorrec);
        flex = true;
        
    } 

    public void MAX(float AngleFlex1co, float AngleFlexlcorrec)
    {

        if (AngleFlex1co > F1)
        {
            F1 = AngleFlex1co;
        }
        if (AngleFlexlcorrec > F2)
        {
            F2 = AngleFlexlcorrec;
        }
        valor = AngleFlex1co;
        valor2 = AngleFlexlcorrec;
        derecha.text = ("" + AngleFlex1co);
        izquierda.text = ("" + AngleFlexlcorrec);
        Repeticiones(valor);
        angulo.text = ("" + F1);//
        angulo2.text = ("" + F2);
     }

    public void Repeticiones(float valor)
    {
        repeticionesnum.text = ("" + cont);
        if (valor < 20 && rep == true)
        {
            if (repeticiones0 == true)
            {
                cont = cont + 1;
            }

            rep = false;
            Debug.Log(cont);

            if (cont == 10)
            {
                Debug.Log("Valor maximo enviado :   " + F1);

            }

        }
        if (valor >20)
        {
            rep = true;
            repeticiones0 = true;
        }

    }
    void Update()
    {

        if (BodySourceManager == null)
        {
            return;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }

        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }

        var floorPlane = _BodyManager.FloorPlanes;
        var newrotation = Quaternion.FromToRotation(new Vector3(floorPlane.X, floorPlane.Y, floorPlane.Z), Vector3.up);


        List<ulong> trackedIds = new List<ulong>();
        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                trackedIds.Add(body.TrackingId);
            }
        }

        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);

        // First delete untracked bodies
        foreach (ulong trackingId in knownIds)
        {
            if (!trackedIds.Contains(trackingId))
            {
                
                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }

        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                if (!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }

                RefreshBodyObject(body, _Bodies[body.TrackingId]);

                Quaternion SpineBase = body.JointOrientations[JointType.SpineBase].Orientation.ChangeQuat(newrotation);
                Quaternion SpineMid = body.JointOrientations[JointType.SpineMid].Orientation.ChangeQuat(newrotation);
                Quaternion SpineShoulder = body.JointOrientations[JointType.SpineShoulder].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderLeft = body.JointOrientations[JointType.ShoulderLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderRight = body.JointOrientations[JointType.ShoulderRight].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowLeft = body.JointOrientations[JointType.ElbowLeft].Orientation.ChangeQuat(newrotation);
                Quaternion WristLeft = body.JointOrientations[JointType.WristLeft].Orientation.ChangeQuat(newrotation);
                Quaternion HandLeft = body.JointOrientations[JointType.HandLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowRight = body.JointOrientations[JointType.ElbowRight].Orientation.ChangeQuat(newrotation);
                Quaternion WristRight = body.JointOrientations[JointType.WristRight].Orientation.ChangeQuat(newrotation);
                Quaternion HandRight = body.JointOrientations[JointType.HandRight].Orientation.ChangeQuat(newrotation);
                Quaternion KneeLeft = body.JointOrientations[JointType.KneeLeft].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleLeft = body.JointOrientations[JointType.AnkleLeft].Orientation.ChangeQuat(newrotation);
                Quaternion KneeRight = body.JointOrientations[JointType.KneeRight].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleRight = body.JointOrientations[JointType.AnkleRight].Orientation.ChangeQuat(newrotation);
                Quaternion FootRight = body.JointOrientations[JointType.FootRight].Orientation.ChangeQuat(newrotation); //
                Quaternion FootLeft = body.JointOrientations[JointType.FootLeft].Orientation.ChangeQuat(newrotation); //
                FootRight.y = 1;
                FootRight.w = 0;
                FootLeft.y = 1;
                FootLeft.w = 0;
                Spine1.transform.rotation = SpineMid * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightArm.transform.rotation = ElbowRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightForeArm.transform.rotation = WristRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightHand.transform.rotation = HandRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftArm.transform.rotation = ElbowLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftForeArm.transform.rotation = WristLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightFoot.transform.rotation = FootRight * Quaternion.AngleAxis(0, new Vector3(0, 0, 1)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0)); // ojo -90
                LeftFoot.transform.rotation = FootLeft * Quaternion.AngleAxis(180, new Vector3(1, 0, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));//
                LeftHand.transform.rotation = HandLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightUpLeg.transform.rotation = KneeRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightLeg.transform.rotation = AnkleRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftUpLeg.transform.rotation = KneeLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftLeg.transform.rotation = AnkleLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

                var moveposition = body.Joints[JointType.SpineMid].Position;
                Master.transform.position = new Vector3(moveposition.X, moveposition.Y, -moveposition.Z);


                ////////////////////////////////////Miembro inferior////////////////////////////////////////////////////////////////////////////////////////////////////    
                // ::::::::::::::::::::::::::  HIP - KNEE EXERCISE HORIZONTAL  :::::::::::::::::::: // 

                if (Input.GetKeyDown(KeyCode.B))
                {
                    Debug.Log("Horizontal Exercise selected");
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", " ::::::   HORIZONTAL EXERCISE   :::::: "); // Guardar los datos 
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

                }
                // ::::::::::::::::::::   SYMMETRY  BODY :::::::::::::::::::: //     

                if (Input.GetKeyDown(KeyCode.C))
                {
                    //variables
                    float KneeeX = body.Joints[JointType.KneeRight].Position.X;
                    float KneeeY = body.Joints[JointType.KneeRight].Position.Y;
                    float KneeeZ = body.Joints[JointType.KneeRight].Position.Z;
                    float lelkneeX = body.Joints[JointType.KneeLeft].Position.X;
                    float lelkneeY = body.Joints[JointType.KneeLeft].Position.Y;
                    float lelkneeZ = body.Joints[JointType.KneeLeft].Position.Z;
                    float AnkleXr = body.Joints[JointType.AnkleRight].Position.X;
                    float AnkleYr = body.Joints[JointType.AnkleRight].Position.Y;
                    float AnkleZr = body.Joints[JointType.AnkleRight].Position.Z;
                    float AnkleXl = body.Joints[JointType.AnkleLeft].Position.X;
                    float AnkleYl = body.Joints[JointType.AnkleLeft].Position.Y;
                    float AnkleZl = body.Joints[JointType.AnkleLeft].Position.Z;
                    float HipXr = body.Joints[JointType.HipRight].Position.X;
                    float HipYr = body.Joints[JointType.HipRight].Position.Y;
                    float HipZr = body.Joints[JointType.HipRight].Position.Z;
                    float HipXl = body.Joints[JointType.HipLeft].Position.X;
                    float HipYl = body.Joints[JointType.HipLeft].Position.Y;
                    float HipZl = body.Joints[JointType.HipLeft].Position.Z;
                    //results- Medicion de la simetria entre cada una de las partes 
                    float Difknee = Mathf.Abs(KneeeY - lelkneeY) * 100; //Diferencia entre Rodilla izquierda y derecha 
                    float DifAnkle = Mathf.Abs(AnkleYr - AnkleYl) * 100; //Diferencia entre Tobillo izquierdo y derecho 
                    float DifHip = Math.Abs(HipYr - HipYl) * 100; //Diferencia cadera 
                    float DifKneeZ = Mathf.Abs(KneeeZ - lelkneeZ) * 100; //Diferencia entre Rodilla derecha y izquierda 
                    float DifAnkleZ = Mathf.Abs(AnkleZr - AnkleZl) * 100; //Diferencia entre entre derecha y izquierda 


                    Debug.Log("Knee Right Heigh = " + KneeeY + "  Knee Left Heigh = " + lelkneeY); //Simetria de la rodilla 
                    Debug.Log("Ankle Right Heigh = " + AnkleYr + "  Ankle Left Heigh = " + AnkleYl); //Simetria del tobillo
                    Debug.Log("Hip Right Heigh = " + HipYr + " Hip Left Height = " + HipYl); //Simetria de la cadera 
                    Debug.Log("Knee Right Depth = " + KneeeZ + "  Knee Left Depth = " + lelkneeZ); //Simetria de la rodilla 
                    Debug.Log("Ankle Right Depth = " + AnkleZr + "  Ankle Left Depth = " + AnkleZl); //Simetria del tobillo 
                    Debug.Log("Hip Right Depth = " + HipZr + "  Hip Left Depth = " + HipZl); //Simetria de la cadera 

                    Debug.Log("Diference between Ankles: " + DifAnkle);
                    Debug.Log("Diference between Hips: " + DifHip);
                    Debug.Log("Diference between Knees: " + Difknee);

                    //File results
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between elbows " + Difknee);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between shoulders " + DifHip);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C: \Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between wrists " + DifAnkleZ);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between elbows " + DifKneeZ);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

                }
                // ::::::::::::::::::::   FLEXION  Hip :::::::::::::::::::: //

                if (flex = true)
                {
                    //Variables Hip 

                    float x11 = body.Joints[JointType.HipRight].Position.X;
                    float y11 = body.Joints[JointType.HipRight].Position.Y;
                    float z11 = body.Joints[JointType.HipRight].Position.Z;
                    float x22 = body.Joints[JointType.HipLeft].Position.X;
                    float y22 = body.Joints[JointType.HipLeft].Position.Y;
                    float z22 = body.Joints[JointType.HipLeft].Position.Z;
                    float x33 = body.Joints[JointType.SpineMid].Position.X;
                    float y33 = body.Joints[JointType.SpineMid].Position.Y;
                    float z33 = body.Joints[JointType.SpineMid].Position.Z;
                    float k11 = body.Joints[JointType.KneeLeft].Position.X;
                    float k22 = body.Joints[JointType.KneeLeft].Position.Y;
                    float k33 = body.Joints[JointType.KneeLeft].Position.Z;
                    float k1l = body.Joints[JointType.KneeRight].Position.X;
                    float k2l = body.Joints[JointType.KneeRight].Position.Y;
                    float k3l = body.Joints[JointType.KneeRight].Position.Z;
                    float u11 = body.Joints[JointType.SpineBase].Position.X;
                    float u22 = body.Joints[JointType.SpineBase].Position.Y;
                    float u33 = body.Joints[JointType.SpineBase].Position.Z;


                    //plane1  (SpineMid-HipRight) 
                    Vector3 vector1 = new Vector3(x22 - x11, y22 - y11, z22 - z11); // Vectorial Result HipRight 
                    Vector3 vector2 = new Vector3(x33 - x11, y33 - y11, z33 - z11);
                    Vector3 Cp1;
                    Cp1 = Vector3.Cross(vector1, vector2); //Producto entre el vector 1 y 2 
                    Vector3 normalvect = new Vector3(Cp1.x, Cp1.y, Cp1.z); //Vector normal 
                    Vector3 Vec_Aux1 = new Vector3(x33 - u11, y33 - u22, z33 - u33);
                    Vector3 newcp;
                    newcp = Vector3.Cross(normalvect, Vec_Aux1);  //Vector resultante = vector normal, vector entre los dos puntos 
                    float daux1 = (newcp.x * (-x33) + newcp.y * (-y33) + newcp.z * (-z33)); //Magnitud del vector resultante
               
                   
                    //Right side projection and result Knee and Hip
                    float lambda_2 = (((-newcp.x) * k1l + (-newcp.y) * k2l + (-newcp.z) * k3l) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 KneeProject = new Vector3(k1l + (lambda_2 * newcp.x), k2l + (lambda_2 * newcp.y), k3l + (lambda_2 * newcp.z));
                    float KneeXr = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                    float KneeYr = body.Joints[JointType.ShoulderRight].Position.Y;
                    float KneeZr = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambda_aux1 = (((-newcp.x) * KneeXr + (-newcp.y) * KneeYr + (-newcp.z) * KneeZr) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Hipproject = new Vector3(KneeXr + (lambda_aux1 * newcp.x), KneeYr + (lambda_aux1 * newcp.y), KneeZr + (lambda_aux1 * newcp.z));
                    Vector3 VectorKnee1 = new Vector3(Hipproject.x - KneeProject.x, Hipproject.y - KneeProject.y, Hipproject.z - KneeProject.z);
                    float AngleFlex1 = Vector3.Angle(Vec_Aux1, VectorKnee1);
                    float AngleFlex1co = 180 - AngleFlex1; //Angulo de flexión

                    //left side Knee and Hip 
                    float lambda_3 = (((-newcp.x) * k11 + (-newcp.y) * k22 + (-newcp.z) * k33) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Kneeproject = new Vector3(k11 + (lambda_3 * newcp.x), k22 + (lambda_3 * newcp.y), k33 + (lambda_3 * newcp.z));
                    float KneeXl = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                    float KneeYl = body.Joints[JointType.ShoulderRight].Position.Y;
                    float KneeZl = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambda_4 = (((-newcp.x) * KneeXl + (-newcp.y) * KneeYl + (-newcp.z) * KneeZl) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Hipproj = new Vector3(KneeXl + (lambda_4 * newcp.x), KneeYl + (lambda_4 * newcp.y), KneeZl + (lambda_4 * newcp.z));
                    Vector3 Hip_A = new Vector3(Kneeproject.x - Hipproj.x, Kneeproject.y - Hipproj.y, Kneeproject.z - Hipproj.z);
                    float AngleFlexl = Vector3.Angle(Vec_Aux1, Hip_A);
                    float AngleFlexlcorrec = 180 - AngleFlexl; // Se le resta a 180 los grados que indique la operación
                    StartCoroutine(Temporizador(AngleFlex1co, AngleFlexlcorrec));
                }
                
            }
        }

    }


    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.SetVertexCount(2);
            lr.material = BoneMaterial;
            lr.SetWidth(0.0f, 0.0f);

            jointObj.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }

        return body;


    }

    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;

            if (_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            Transform jointObj = bodyObject.transform.Find(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if (targetJoint.HasValue)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }
        }
    }

    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
            case Kinect.TrackingState.Tracked:
                return Color.green;

            case Kinect.TrackingState.Inferred:
                return Color.red;

            default:
                return Color.black;
        }
    }

    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}

using UnityEngine;
using System.Collections;
using Windows.Data;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using Windows.Kinect;
using System.Linq;
using System.IO;
using System;
using UnityEngine.UI;


public class BodySourceView : MonoBehaviour
{
    public Material BoneMaterial;
    public GameObject BodySourceManager;
    public GameObject _personaje1_Unity;
    public GameObject Master;
    public GameObject Reference;
    public GameObject Hips;
    public GameObject LeftUpLeg;
    public GameObject LeftLeg;
    public GameObject RightUpLeg;
    public GameObject RightLeg;
    public GameObject Spine;
    public GameObject Spine1;
    public GameObject Spine2;
    public GameObject LeftShoulder;
    public GameObject LeftArm;
    public GameObject LeftForeArm;
    public GameObject LeftHand;
    public GameObject Neck;
    public GameObject Head;
    public GameObject RightShoulder;
    public GameObject RightArm;
    public GameObject RightForeArm;
    public GameObject RightHand;
    public GameObject LeftFoot; //
    public GameObject RightFoot; //
    public GameObject RightAnkle; //
    public GameObject LeftAnkle; //
    public float F1 = 0;
    public float F2 = 0;
    public Text derecha = null; //Angulo Actual Derecho
    public Text izquierda = null; //Angulo Actual Izquierdo 

    public float maxtot = 0;
    public int cont = -1;

    public bool rep = true;

    public Text angulo = null; //Angulo_Max_Derecho
    public Text angulo2 = null; //Angulo_Max_Izquierdo

    public bool angulonum = false;
    public GameObject semaforo;
    public GameObject semaforo2;
    public Text copiaej = null;
    public float valor;
    public float valor2;

    public Text repeticionesnum;
    public bool repeticiones0 = false;
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;


    // Canvax Maximos y minimos value
    /// </summary>

    public bool flex = false;
    public void ButtonClick()
    {
        flex = true;
        angulonum = true;
        StartCoroutine(Seemaforo());
    }

    /// </summary>

    // Mapeo "puntos" del Cuerpo Humano

    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    public void Start()
    {

        _personaje1_Unity = GameObject.Find("personaje1_Unity");
        Master = GameObject.Find("master");
        Reference = GameObject.Find("Reference");
        Hips = GameObject.Find("Hips");
        LeftUpLeg = GameObject.Find("LeftUpLeg");
        LeftLeg = GameObject.Find("LeftLeg");
        RightUpLeg = GameObject.Find("RightUpLeg");
        RightLeg = GameObject.Find("RightLeg");
        Spine = GameObject.Find("Spine");
        Spine1 = GameObject.Find("Spine1");
        Spine2 = GameObject.Find("Spine2");
        LeftShoulder = GameObject.Find("LeftShoulder");
        LeftArm = GameObject.Find("LeftArm");
        LeftForeArm = GameObject.Find("LeftForeArm");
        LeftHand = GameObject.Find("LeftHand");
        Neck = GameObject.Find("Neck");
        Head = GameObject.Find("Head");
        RightShoulder = GameObject.Find("RightShoulder");
        RightArm = GameObject.Find("RightArm");
        RightForeArm = GameObject.Find("RightForeArm");
        RightHand = GameObject.Find("RightHand");
        RightAnkle = GameObject.Find("RightAnkle"); //
        LeftAnkle = GameObject.Find("LeftAnkle");   //
        RightFoot = GameObject.Find("RightFoot");  //
        LeftFoot = GameObject.Find("LeftFoot");    //

    }
    IEnumerator Seemaforo()
    {
        copiaej.text = "Realice el ejercicio propuesto";
        yield return new WaitForSeconds(5);
        Destroy(semaforo);
        copiaej.text = "Ahora es su turno!";
        semaforo2.SetActive(true);
    }

    IEnumerator Temporizador(float AngleFlex1co, float AngleFlexlcorrec)
    {
        flex = false;
        yield return new WaitForSeconds(0.5f);// Mira angulos FLex Cada segundo miembro inferior
        MAX(AngleFlex1co, AngleFlexlcorrec);
        flex = true;

    }

    public void MAX(float AngleFlex1co, float AngleFlexlcorrec)
    {

        if (AngleFlex1co > F1)
        {
            F1 = AngleFlex1co;
        }
        if (AngleFlexlcorrec > F2)
        {
            F2 = AngleFlexlcorrec;
        }
        valor = AngleFlex1co;
        valor2 = AngleFlexlcorrec;
        derecha.text = ("" + AngleFlex1co);
        izquierda.text = ("" + AngleFlexlcorrec);
        Repeticiones(valor);
        angulo.text = ("" + F1);//
        angulo2.text = ("" + F2);
    }

    public void Repeticiones(float valor)
    {
        repeticionesnum.text = ("" + cont);
        if (valor < 20 && rep == true)
        {
            if (repeticiones0 == true)
            {
                cont = cont + 1;
            }
            rep = false;
            Debug.Log(cont);

            if (cont == 10)
            {
                Debug.Log("Valor maximo enviado :   " + F1);

            }

        }
        if (valor > 20)
        {
            rep = true;
            repeticiones0 = true;
        }
    }
    void Update()
    {

        if (BodySourceManager == null)
        {
            return;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }

        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }

        var floorPlane = _BodyManager.FloorPlanes;
        var newrotation = Quaternion.FromToRotation(new Vector3(floorPlane.X, floorPlane.Y, floorPlane.Z), Vector3.up);


        List<ulong> trackedIds = new List<ulong>();
        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                trackedIds.Add(body.TrackingId);
            }
        }

        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);

        // First delete untracked bodies
        foreach (ulong trackingId in knownIds)
        {
            if (!trackedIds.Contains(trackingId))
            {

                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }

        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                if (!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }

                RefreshBodyObject(body, _Bodies[body.TrackingId]);

                Quaternion SpineBase = body.JointOrientations[JointType.SpineBase].Orientation.ChangeQuat(newrotation);
                Quaternion SpineMid = body.JointOrientations[JointType.SpineMid].Orientation.ChangeQuat(newrotation);
                Quaternion SpineShoulder = body.JointOrientations[JointType.SpineShoulder].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderLeft = body.JointOrientations[JointType.ShoulderLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderRight = body.JointOrientations[JointType.ShoulderRight].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowLeft = body.JointOrientations[JointType.ElbowLeft].Orientation.ChangeQuat(newrotation);
                Quaternion WristLeft = body.JointOrientations[JointType.WristLeft].Orientation.ChangeQuat(newrotation);
                Quaternion HandLeft = body.JointOrientations[JointType.HandLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowRight = body.JointOrientations[JointType.ElbowRight].Orientation.ChangeQuat(newrotation);
                Quaternion WristRight = body.JointOrientations[JointType.WristRight].Orientation.ChangeQuat(newrotation);
                Quaternion HandRight = body.JointOrientations[JointType.HandRight].Orientation.ChangeQuat(newrotation);
                Quaternion KneeLeft = body.JointOrientations[JointType.KneeLeft].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleLeft = body.JointOrientations[JointType.AnkleLeft].Orientation.ChangeQuat(newrotation);
                Quaternion KneeRight = body.JointOrientations[JointType.KneeRight].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleRight = body.JointOrientations[JointType.AnkleRight].Orientation.ChangeQuat(newrotation);
                Quaternion FootRight = body.JointOrientations[JointType.FootRight].Orientation.ChangeQuat(newrotation); //
                Quaternion FootLeft = body.JointOrientations[JointType.FootLeft].Orientation.ChangeQuat(newrotation); //
                FootRight.y = 1;
                FootRight.w = 0;
                FootLeft.y = 1;
                FootLeft.w = 0;
                Spine1.transform.rotation = SpineMid * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightArm.transform.rotation = ElbowRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightForeArm.transform.rotation = WristRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightHand.transform.rotation = HandRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftArm.transform.rotation = ElbowLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftForeArm.transform.rotation = WristLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightFoot.transform.rotation = FootRight * Quaternion.AngleAxis(0, new Vector3(0, 0, 1)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0)); // ojo -90
                LeftFoot.transform.rotation = FootLeft * Quaternion.AngleAxis(180, new Vector3(1, 0, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));//
                LeftHand.transform.rotation = HandLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightUpLeg.transform.rotation = KneeRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightLeg.transform.rotation = AnkleRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftUpLeg.transform.rotation = KneeLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftLeg.transform.rotation = AnkleLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

                var moveposition = body.Joints[JointType.SpineMid].Position;
                Master.transform.position = new Vector3(moveposition.X, moveposition.Y, -moveposition.Z);


                ////////////////////////////////////Miembro inferior////////////////////////////////////////////////////////////////////////////////////////////////////    
                // ::::::::::::::::::::::::::  HIP - KNEE EXERCISE HORIZONTAL  :::::::::::::::::::: // 

                if (Input.GetKeyDown(KeyCode.B))
                {
                    Debug.Log("Horizontal Exercise selected");
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", " ::::::   HORIZONTAL EXERCISE   :::::: "); // Guardar los datos 
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

                }
                // ::::::::::::::::::::   SYMMETRY  BODY :::::::::::::::::::: //     

                if (Input.GetKeyDown(KeyCode.C))
                {
                    //variables
                    float KneeeX = body.Joints[JointType.KneeRight].Position.X;
                    float KneeeY = body.Joints[JointType.KneeRight].Position.Y;
                    float KneeeZ = body.Joints[JointType.KneeRight].Position.Z;
                    float lelkneeX = body.Joints[JointType.KneeLeft].Position.X;
                    float lelkneeY = body.Joints[JointType.KneeLeft].Position.Y;
                    float lelkneeZ = body.Joints[JointType.KneeLeft].Position.Z;
                    float AnkleXr = body.Joints[JointType.AnkleRight].Position.X;
                    float AnkleYr = body.Joints[JointType.AnkleRight].Position.Y;
                    float AnkleZr = body.Joints[JointType.AnkleRight].Position.Z;
                    float AnkleXl = body.Joints[JointType.AnkleLeft].Position.X;
                    float AnkleYl = body.Joints[JointType.AnkleLeft].Position.Y;
                    float AnkleZl = body.Joints[JointType.AnkleLeft].Position.Z;
                    float HipXr = body.Joints[JointType.HipRight].Position.X;
                    float HipYr = body.Joints[JointType.HipRight].Position.Y;
                    float HipZr = body.Joints[JointType.HipRight].Position.Z;
                    float HipXl = body.Joints[JointType.HipLeft].Position.X;
                    float HipYl = body.Joints[JointType.HipLeft].Position.Y;
                    float HipZl = body.Joints[JointType.HipLeft].Position.Z;
                    //results- Medicion de la simetria entre cada una de las partes 
                    float Difknee = Mathf.Abs(KneeeY - lelkneeY) * 100; //Diferencia entre Rodilla izquierda y derecha 
                    float DifAnkle = Mathf.Abs(AnkleYr - AnkleYl) * 100; //Diferencia entre Tobillo izquierdo y derecho 
                    float DifHip = Math.Abs(HipYr - HipYl) * 100; //Diferencia cadera 
                    float DifKneeZ = Mathf.Abs(KneeeZ - lelkneeZ) * 100; //Diferencia entre Rodilla derecha y izquierda 
                    float DifAnkleZ = Mathf.Abs(AnkleZr - AnkleZl) * 100; //Diferencia entre entre derecha y izquierda 


                    Debug.Log("Knee Right Heigh = " + KneeeY + "  Knee Left Heigh = " + lelkneeY); //Simetria de la rodilla 
                    Debug.Log("Ankle Right Heigh = " + AnkleYr + "  Ankle Left Heigh = " + AnkleYl); //Simetria del tobillo
                    Debug.Log("Hip Right Heigh = " + HipYr + " Hip Left Height = " + HipYl); //Simetria de la cadera 
                    Debug.Log("Knee Right Depth = " + KneeeZ + "  Knee Left Depth = " + lelkneeZ); //Simetria de la rodilla 
                    Debug.Log("Ankle Right Depth = " + AnkleZr + "  Ankle Left Depth = " + AnkleZl); //Simetria del tobillo 
                    Debug.Log("Hip Right Depth = " + HipZr + "  Hip Left Depth = " + HipZl); //Simetria de la cadera 

                    Debug.Log("Diference between Ankles: " + DifAnkle);
                    Debug.Log("Diference between Hips: " + DifHip);
                    Debug.Log("Diference between Knees: " + Difknee);

                    //File results
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between elbows " + Difknee);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between shoulders " + DifHip);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C: \Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between wrists " + DifAnkleZ);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between elbows " + DifKneeZ);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

                }
                // ::::::::::::::::::::   FLEXION  Hip :::::::::::::::::::: //

                if (flex = true)
                {
                    //Variables Hip 

                    float x11 = body.Joints[JointType.HipRight].Position.X;
                    float y11 = body.Joints[JointType.HipRight].Position.Y;
                    float z11 = body.Joints[JointType.HipRight].Position.Z;
                    float x22 = body.Joints[JointType.HipLeft].Position.X;
                    float y22 = body.Joints[JointType.HipLeft].Position.Y;
                    float z22 = body.Joints[JointType.HipLeft].Position.Z;
                    float x33 = body.Joints[JointType.SpineMid].Position.X;
                    float y33 = body.Joints[JointType.SpineMid].Position.Y;
                    float z33 = body.Joints[JointType.SpineMid].Position.Z;
                    float k11 = body.Joints[JointType.KneeLeft].Position.X;
                    float k22 = body.Joints[JointType.KneeLeft].Position.Y;
                    float k33 = body.Joints[JointType.KneeLeft].Position.Z;
                    float k1l = body.Joints[JointType.KneeRight].Position.X;
                    float k2l = body.Joints[JointType.KneeRight].Position.Y;
                    float k3l = body.Joints[JointType.KneeRight].Position.Z;
                    float u11 = body.Joints[JointType.SpineBase].Position.X;
                    float u22 = body.Joints[JointType.SpineBase].Position.Y;
                    float u33 = body.Joints[JointType.SpineBase].Position.Z;


                    //plane1  (SpineMid-HipRight) 
                    Vector3 vector1 = new Vector3(x22 - x11, y22 - y11, z22 - z11); // Vectorial Result HipRight 
                    Vector3 vector2 = new Vector3(x33 - x11, y33 - y11, z33 - z11);
                    Vector3 Cp1;
                    Cp1 = Vector3.Cross(vector1, vector2); //Producto entre el vector 1 y 2 
                    Vector3 normalvect = new Vector3(Cp1.x, Cp1.y, Cp1.z); //Vector normal 
                    Vector3 Vec_Aux1 = new Vector3(x33 - u11, y33 - u22, z33 - u33);
                    Vector3 newcp;
                    newcp = Vector3.Cross(normalvect, Vec_Aux1);  //Vector resultante = vector normal, vector entre los dos puntos 
                    float daux1 = (newcp.x * (-x33) + newcp.y * (-y33) + newcp.z * (-z33)); //Magnitud del vector resultante


                    //Right side projection and result Knee and Hip
                    float lambda_2 = (((-newcp.x) * k1l + (-newcp.y) * k2l + (-newcp.z) * k3l) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 KneeProject = new Vector3(k1l + (lambda_2 * newcp.x), k2l + (lambda_2 * newcp.y), k3l + (lambda_2 * newcp.z));
                    float KneeXr = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                    float KneeYr = body.Joints[JointType.ShoulderRight].Position.Y;
                    float KneeZr = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambda_aux1 = (((-newcp.x) * KneeXr + (-newcp.y) * KneeYr + (-newcp.z) * KneeZr) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Hipproject = new Vector3(KneeXr + (lambda_aux1 * newcp.x), KneeYr + (lambda_aux1 * newcp.y), KneeZr + (lambda_aux1 * newcp.z));
                    Vector3 VectorKnee1 = new Vector3(Hipproject.x - KneeProject.x, Hipproject.y - KneeProject.y, Hipproject.z - KneeProject.z);
                    float AngleFlex1 = Vector3.Angle(Vec_Aux1, VectorKnee1);
                    float AngleFlex1co = 180 - AngleFlex1; //Angulo de flexión

                    //left side Knee and Hip 
                    float lambda_3 = (((-newcp.x) * k11 + (-newcp.y) * k22 + (-newcp.z) * k33) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Kneeproject = new Vector3(k11 + (lambda_3 * newcp.x), k22 + (lambda_3 * newcp.y), k33 + (lambda_3 * newcp.z));
                    float KneeXl = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                    float KneeYl = body.Joints[JointType.ShoulderRight].Position.Y;
                    float KneeZl = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambda_4 = (((-newcp.x) * KneeXl + (-newcp.y) * KneeYl + (-newcp.z) * KneeZl) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Hipproj = new Vector3(KneeXl + (lambda_4 * newcp.x), KneeYl + (lambda_4 * newcp.y), KneeZl + (lambda_4 * newcp.z));
                    Vector3 Hip_A = new Vector3(Kneeproject.x - Hipproj.x, Kneeproject.y - Hipproj.y, Kneeproject.z - Hipproj.z);
                    float AngleFlexl = Vector3.Angle(Vec_Aux1, Hip_A);
                    float AngleFlexlcorrec = 180 - AngleFlexl; // Se le resta a 180 los grados que indique la operación
                    StartCoroutine(Temporizador(AngleFlex1co, AngleFlexlcorrec));

                    ///////////////////////// Knee ///////////////////////////////////

                    float HipXr = body.Joints[JointType.HipRight].Position.X;
                    float HipYr = body.Joints[JointType.HipRight].Position.Y;
                    float HipZr = body.Joints[JointType.HipRight].Position.Z;
                    float KneeeX = body.Joints[JointType.KneeRight].Position.X;
                    float KneeeY = body.Joints[JointType.KneeRight].Position.Y;
                    float KneeeZ = body.Joints[JointType.KneeRight].Position.Z;
                    float AnkleXr = body.Joints[JointType.AnkleRight].Position.X;
                    float AnkleYr = body.Joints[JointType.AnkleRight].Position.Y;
                    float AnkleZr = body.Joints[JointType.AnkleRight].Position.Z;
                    float HipXl = body.Joints[JointType.HipLeft].Position.X;
                    float HipYl = body.Joints[JointType.HipLeft].Position.Y;
                    float HipZl = body.Joints[JointType.HipLeft].Position.Z;
                    float lelKneeX = body.Joints[JointType.KneeLeft].Position.X;
                    float lelKneeY = body.Joints[JointType.KneeLeft].Position.Y;
                    float lelKneeZ = body.Joints[JointType.KneeLeft].Position.Z;
                    float AnkleXl = body.Joints[JointType.AnkleLeft].Position.X;
                    float AnkleYl = body.Joints[JointType.AnkleLeft].Position.Y;
                    float AnkleZl = body.Joints[JointType.AnkleLeft].Position.Z;

                    //right side results Knee
                    Vector3 VecHKR = new Vector3(HipXr - KneeeX, HipYr - KneeeY, HipZr - KneeeZ); // Vector cadera- rodilla 
                    Vector3 VecKWR = new Vector3(AnkleXr - KneeeX, AnkleYr - KneeeY, AnkleZr - KneeeZ); // Vector rodilla- tobillo 
                    float AngleKneeR = Vector3.Angle(VecHKR, VecKWR); //Vector resultante 
                    float AngleKneeR_Co = 180 - AngleKneeR;
                    //Debug.Log(" Angle right Knee: " + AngleKneeR_Co); // Impresión del vector-- lo muestra angulo flexión de la rodilla 

                    //left side results Knee
                    Vector3 VecHKL = new Vector3(HipXl - lelKneeX, HipYl - lelKneeY, HipZl - lelKneeZ);
                    Vector3 VecHWL = new Vector3(AnkleXl - lelKneeX, AnkleYl - lelKneeY, AnkleZl - lelKneeZ);
                    float AngleKneeL = Vector3.Angle(VecHKL, VecHWL);
                    float AngleKneeL_Co = 180 - AngleKneeL;
                    //Debug.Log(" Angle left Knee: " + AngleKneeL_Co);
                    //StartCoroutine(Temporizador1(AngleKneeR_Co, AngleKneeL_Co));
                }

            }
        }

    }


    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.SetVertexCount(2);
            lr.material = BoneMaterial;
            lr.SetWidth(0.0f, 0.0f);

            jointObj.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }

        return body;


    }

    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;

            if (_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            Transform jointObj = bodyObject.transform.Find(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if (targetJoint.HasValue)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }
        }
    }

    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
            case Kinect.TrackingState.Tracked:
                return Color.green;

            case Kinect.TrackingState.Inferred:
                return Color.red;

            default:
                return Color.black;
        }
    }

    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}


using UnityEngine;
using System.Collections;
using Windows.Data;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using Windows.Kinect;
using System.Linq;
using System.IO;
using System;
using UnityEngine.UI;


public class BodySourceView : MonoBehaviour
{
    public Material BoneMaterial;
    public GameObject BodySourceManager;
    public GameObject _personaje1_Unity;
    public GameObject Master;
    public GameObject Reference;
    public GameObject Hips;
    public GameObject LeftUpLeg;
    public GameObject LeftLeg;
    public GameObject RightUpLeg;
    public GameObject RightLeg;
    public GameObject Spine;
    public GameObject Spine1;
    public GameObject Spine2;
    public GameObject LeftShoulder;
    public GameObject LeftArm;
    public GameObject LeftForeArm;
    public GameObject LeftHand;
    public GameObject Neck;
    public GameObject Head;
    public GameObject RightShoulder;
    public GameObject RightArm;
    public GameObject RightForeArm;
    public GameObject RightHand;
    public GameObject LeftFoot; //
    public GameObject RightFoot; //
    public GameObject RightAnkle; //
    public GameObject LeftAnkle; //
    public float F1 = 0;
    public float F2 = 0;
    public Text derecha = null; //Angulo Actual Derecho
    public Text izquierda = null; //Angulo Actual Izquierdo 
    public float maxtot = 0;
    public int cont = -1;
    public bool rep = true;
    public Text angulo = null; //Angulo_Max_Derecho
    public Text angulo2 = null; //Angulo_Max_Izquierdo
    public bool angulonum = false;
    public GameObject semaforo;
    public GameObject semaforo2;
    public Text copiaej = null;
    public float valor;
    public float valor2;
    public Text repeticionesnum;
    public bool repeticiones0=false;
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;


// Canvax Maximos y minimos value
/// </summary>

    public bool flex = false;
    public void Flex()
    {
        flex = true;
        StartCoroutine(Seemaforo());
   }

/// </summary>

// Mapeo "puntos" del Cuerpo Humano

private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    public void Start()
    {
        
        _personaje1_Unity = GameObject.Find("personaje1_Unity");
        Master = GameObject.Find("master");
        Reference = GameObject.Find("Reference");
        Hips = GameObject.Find("Hips");
        LeftUpLeg = GameObject.Find("LeftUpLeg");
        LeftLeg = GameObject.Find("LeftLeg");
        RightUpLeg = GameObject.Find("RightUpLeg");
        RightLeg = GameObject.Find("RightLeg");
        Spine = GameObject.Find("Spine");
        Spine1 = GameObject.Find("Spine1");
        Spine2 = GameObject.Find("Spine2");
        LeftShoulder = GameObject.Find("LeftShoulder");
        LeftArm = GameObject.Find("LeftArm");
        LeftForeArm = GameObject.Find("LeftForeArm");
        LeftHand = GameObject.Find("LeftHand");
        Neck = GameObject.Find("Neck");
        Head = GameObject.Find("Head");
        RightShoulder = GameObject.Find("RightShoulder");
        RightArm = GameObject.Find("RightArm");
        RightForeArm = GameObject.Find("RightForeArm");
        RightHand = GameObject.Find("RightHand");
        RightAnkle = GameObject.Find("RightAnkle"); //
        LeftAnkle = GameObject.Find("LeftAnkle");   //
        RightFoot = GameObject.Find("RightFoot");  //
        LeftFoot = GameObject.Find("LeftFoot");    //
        
    }
    IEnumerator Seemaforo()
    {
        copiaej.text = "Realice el ejercicio propuesto";
        yield return new WaitForSeconds(5);
        Destroy(semaforo);
        copiaej.text = "Ahora es su turno!";
        semaforo2.SetActive(true);
    }
    
    IEnumerator Temporizador(float AngleFlex1co, float AngleFlexlcorrec)
    {
        flex = false;
        yield return new WaitForSeconds(0.5f);// Mira angulos FLex Cada segundo miembro inferior
        MAX(AngleFlex1co, AngleFlexlcorrec);
        flex = true;
        
    } 

    public void MAX(float AngleFlex1co, float AngleFlexlcorrec)
    {

        if (AngleFlex1co > F1)
        {
            F1 = AngleFlex1co;
        }
        if (AngleFlexlcorrec > F2)
        {
            F2 = AngleFlexlcorrec;
        }
        valor = AngleFlex1co;
        valor2 = AngleFlexlcorrec;
        derecha.text = ("" + AngleFlex1co);
        izquierda.text = ("" + AngleFlexlcorrec);
        Repeticiones(valor);
        angulo.text = ("" + F1);//
        angulo2.text = ("" + F2);
     }

    public void Repeticiones(float valor)
    {
        repeticionesnum.text = ("" + cont);
        if (valor < 20 && rep == true)
        {
            if (repeticiones0 == true)
            {
                cont = cont + 1;
            }
            
            rep = false;
            Debug.Log(cont);

            if (cont == 10)
            {
                Debug.Log("Valor maximo enviado :   " + F1);
                Debug.Log("Valor maximo enviado :   " + F2);
            }

        }
        if (valor >20)
        {
            rep = true;
            repeticiones0 = true;
        }

    }
    void Update()
    {

        if (BodySourceManager == null)
        {
            return;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }

        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }

        var floorPlane = _BodyManager.FloorPlanes;
        var newrotation = Quaternion.FromToRotation(new Vector3(floorPlane.X, floorPlane.Y, floorPlane.Z), Vector3.up);


        List<ulong> trackedIds = new List<ulong>();
        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                trackedIds.Add(body.TrackingId);
            }
        }

        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);

        // First delete untracked bodies
        foreach (ulong trackingId in knownIds)
        {
            if (!trackedIds.Contains(trackingId))
            {
                
                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }

        foreach (var body in data)
        {
            if (body == null)
            {
                continue;
            }

            if (body.IsTracked)
            {
                if (!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }

                RefreshBodyObject(body, _Bodies[body.TrackingId]);

                Quaternion SpineBase = body.JointOrientations[JointType.SpineBase].Orientation.ChangeQuat(newrotation);
                Quaternion SpineMid = body.JointOrientations[JointType.SpineMid].Orientation.ChangeQuat(newrotation);
                Quaternion SpineShoulder = body.JointOrientations[JointType.SpineShoulder].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderLeft = body.JointOrientations[JointType.ShoulderLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ShoulderRight = body.JointOrientations[JointType.ShoulderRight].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowLeft = body.JointOrientations[JointType.ElbowLeft].Orientation.ChangeQuat(newrotation);
                Quaternion WristLeft = body.JointOrientations[JointType.WristLeft].Orientation.ChangeQuat(newrotation);
                Quaternion HandLeft = body.JointOrientations[JointType.HandLeft].Orientation.ChangeQuat(newrotation);
                Quaternion ElbowRight = body.JointOrientations[JointType.ElbowRight].Orientation.ChangeQuat(newrotation);
                Quaternion WristRight = body.JointOrientations[JointType.WristRight].Orientation.ChangeQuat(newrotation);
                Quaternion HandRight = body.JointOrientations[JointType.HandRight].Orientation.ChangeQuat(newrotation);
                Quaternion KneeLeft = body.JointOrientations[JointType.KneeLeft].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleLeft = body.JointOrientations[JointType.AnkleLeft].Orientation.ChangeQuat(newrotation);
                Quaternion KneeRight = body.JointOrientations[JointType.KneeRight].Orientation.ChangeQuat(newrotation);
                Quaternion AnkleRight = body.JointOrientations[JointType.AnkleRight].Orientation.ChangeQuat(newrotation);
                Quaternion FootRight = body.JointOrientations[JointType.FootRight].Orientation.ChangeQuat(newrotation); //
                Quaternion FootLeft = body.JointOrientations[JointType.FootLeft].Orientation.ChangeQuat(newrotation); //
                FootRight.y = 1;
                FootRight.w = 0;
                FootLeft.y = 1;
                FootLeft.w = 0;
                Spine1.transform.rotation = SpineMid * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightArm.transform.rotation = ElbowRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightForeArm.transform.rotation = WristRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightHand.transform.rotation = HandRight * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftArm.transform.rotation = ElbowLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftForeArm.transform.rotation = WristLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) *
                    Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightFoot.transform.rotation = FootRight * Quaternion.AngleAxis(0, new Vector3(0, 0, 1)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0)); // ojo -90
                LeftFoot.transform.rotation = FootLeft * Quaternion.AngleAxis(180, new Vector3(1, 0, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 1, 0));//
                LeftHand.transform.rotation = HandLeft * Quaternion.AngleAxis(180, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                RightUpLeg.transform.rotation = KneeRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                RightLeg.transform.rotation = AnkleRight * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
                LeftUpLeg.transform.rotation = KneeLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                LeftLeg.transform.rotation = AnkleLeft * Quaternion.AngleAxis(90, new Vector3(0, 1, 0)) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));

                var moveposition = body.Joints[JointType.SpineMid].Position;
                Master.transform.position = new Vector3(moveposition.X, moveposition.Y, -moveposition.Z);


                ////////////////////////////////////Miembro inferior////////////////////////////////////////////////////////////////////////////////////////////////////    
                // ::::::::::::::::::::::::::  HIP - KNEE EXERCISE HORIZONTAL  :::::::::::::::::::: // 

                if (Input.GetKeyDown(KeyCode.B))
                {
                    Debug.Log("Horizontal Exercise selected");
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", " ::::::   HORIZONTAL EXERCISE   :::::: "); // Guardar los datos 
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

                }
                // ::::::::::::::::::::   SYMMETRY  BODY :::::::::::::::::::: //     

                if (Input.GetKeyDown(KeyCode.C))
                {
                    //variables
                    float KneeeX = body.Joints[JointType.KneeRight].Position.X;
                    float KneeeY = body.Joints[JointType.KneeRight].Position.Y;
                    float KneeeZ = body.Joints[JointType.KneeRight].Position.Z;
                    float lelkneeX = body.Joints[JointType.KneeLeft].Position.X;
                    float lelkneeY = body.Joints[JointType.KneeLeft].Position.Y;
                    float lelkneeZ = body.Joints[JointType.KneeLeft].Position.Z;
                    float AnkleXr = body.Joints[JointType.AnkleRight].Position.X;
                    float AnkleYr = body.Joints[JointType.AnkleRight].Position.Y;
                    float AnkleZr = body.Joints[JointType.AnkleRight].Position.Z;
                    float AnkleXl = body.Joints[JointType.AnkleLeft].Position.X;
                    float AnkleYl = body.Joints[JointType.AnkleLeft].Position.Y;
                    float AnkleZl = body.Joints[JointType.AnkleLeft].Position.Z;
                    float HipXr = body.Joints[JointType.HipRight].Position.X;
                    float HipYr = body.Joints[JointType.HipRight].Position.Y;
                    float HipZr = body.Joints[JointType.HipRight].Position.Z;
                    float HipXl = body.Joints[JointType.HipLeft].Position.X;
                    float HipYl = body.Joints[JointType.HipLeft].Position.Y;
                    float HipZl = body.Joints[JointType.HipLeft].Position.Z;
                    //results- Medicion de la simetria entre cada una de las partes 
                    float Difknee = Mathf.Abs(KneeeY - lelkneeY) * 100; //Diferencia entre Rodilla izquierda y derecha 
                    float DifAnkle = Mathf.Abs(AnkleYr - AnkleYl) * 100; //Diferencia entre Tobillo izquierdo y derecho 
                    float DifHip = Math.Abs(HipYr - HipYl) * 100; //Diferencia cadera 
                    float DifKneeZ = Mathf.Abs(KneeeZ - lelkneeZ) * 100; //Diferencia entre Rodilla derecha y izquierda 
                    float DifAnkleZ = Mathf.Abs(AnkleZr - AnkleZl) * 100; //Diferencia entre entre derecha y izquierda 


                    Debug.Log("Knee Right Heigh = " + KneeeY + "  Knee Left Heigh = " + lelkneeY); //Simetria de la rodilla 
                    Debug.Log("Ankle Right Heigh = " + AnkleYr + "  Ankle Left Heigh = " + AnkleYl); //Simetria del tobillo
                    Debug.Log("Hip Right Heigh = " + HipYr + " Hip Left Height = " + HipYl); //Simetria de la cadera 
                    Debug.Log("Knee Right Depth = " + KneeeZ + "  Knee Left Depth = " + lelkneeZ); //Simetria de la rodilla 
                    Debug.Log("Ankle Right Depth = " + AnkleZr + "  Ankle Left Depth = " + AnkleZl); //Simetria del tobillo 
                    Debug.Log("Hip Right Depth = " + HipZr + "  Hip Left Depth = " + HipZl); //Simetria de la cadera 

                    Debug.Log("Diference between Ankles: " + DifAnkle);
                    Debug.Log("Diference between Hips: " + DifHip);
                    Debug.Log("Diference between Knees: " + Difknee);

                    //File results
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between wrists: " + DifAnkle);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between elbows " + Difknee);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Height difference between shoulders " + DifHip);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C: \Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between wrists " + DifAnkleZ);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", "Depth difference between elbows " + DifKneeZ);
                    File.AppendAllText(@"C:\Users\admin\Documents\Carpeta_Resultados\Resultados.txt", Environment.NewLine);

                }
                // ::::::::::::::::::::   FLEXION  Hip :::::::::::::::::::: //

                if (flex = true)
                {
                    //Variables Hip 

                    float x11 = body.Joints[JointType.HipRight].Position.X;
                    float y11 = body.Joints[JointType.HipRight].Position.Y;
                    float z11 = body.Joints[JointType.HipRight].Position.Z;
                    float x22 = body.Joints[JointType.HipLeft].Position.X;
                    float y22 = body.Joints[JointType.HipLeft].Position.Y;
                    float z22 = body.Joints[JointType.HipLeft].Position.Z;
                    float x33 = body.Joints[JointType.SpineMid].Position.X;
                    float y33 = body.Joints[JointType.SpineMid].Position.Y;
                    float z33 = body.Joints[JointType.SpineMid].Position.Z;
                    float k11 = body.Joints[JointType.KneeLeft].Position.X;
                    float k22 = body.Joints[JointType.KneeLeft].Position.Y;
                    float k33 = body.Joints[JointType.KneeLeft].Position.Z;
                    float k1l = body.Joints[JointType.KneeRight].Position.X;
                    float k2l = body.Joints[JointType.KneeRight].Position.Y;
                    float k3l = body.Joints[JointType.KneeRight].Position.Z;
                    float u11 = body.Joints[JointType.SpineBase].Position.X;
                    float u22 = body.Joints[JointType.SpineBase].Position.Y;
                    float u33 = body.Joints[JointType.SpineBase].Position.Z;


                    //plane1  (SpineMid-HipRight) 
                    Vector3 vector1 = new Vector3(x22 - x11, y22 - y11, z22 - z11); // Vectorial Result HipRight 
                    Vector3 vector2 = new Vector3(x33 - x11, y33 - y11, z33 - z11);
                    Vector3 Cp1;
                    Cp1 = Vector3.Cross(vector1, vector2); //Producto entre el vector 1 y 2 
                    Vector3 normalvect = new Vector3(Cp1.x, Cp1.y, Cp1.z); //Vector normal 
                    Vector3 Vec_Aux1 = new Vector3(x33 - u11, y33 - u22, z33 - u33);
                    Vector3 newcp;
                    newcp = Vector3.Cross(normalvect, Vec_Aux1);  //Vector resultante = vector normal, vector entre los dos puntos 
                    float daux1 = (newcp.x * (-x33) + newcp.y * (-y33) + newcp.z * (-z33)); //Magnitud del vector resultante
               
                   
                    //Right side projection and result Knee and Hip
                    float lambda_2 = (((-newcp.x) * k1l + (-newcp.y) * k2l + (-newcp.z) * k3l) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 KneeProject = new Vector3(k1l + (lambda_2 * newcp.x), k2l + (lambda_2 * newcp.y), k3l + (lambda_2 * newcp.z));
                    float KneeXr = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                    float KneeYr = body.Joints[JointType.ShoulderRight].Position.Y;
                    float KneeZr = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambda_aux1 = (((-newcp.x) * KneeXr + (-newcp.y) * KneeYr + (-newcp.z) * KneeZr) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Hipproject = new Vector3(KneeXr + (lambda_aux1 * newcp.x), KneeYr + (lambda_aux1 * newcp.y), KneeZr + (lambda_aux1 * newcp.z));
                    Vector3 VectorKnee1 = new Vector3(Hipproject.x - KneeProject.x, Hipproject.y - KneeProject.y, Hipproject.z - KneeProject.z);
                    float AngleFlex1 = Vector3.Angle(Vec_Aux1, VectorKnee1);
                    float AngleFlex1co = 180 - AngleFlex1; //Angulo de flexión

                    //left side Knee and Hip 
                    float lambda_3 = (((-newcp.x) * k11 + (-newcp.y) * k22 + (-newcp.z) * k33) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Kneeproject = new Vector3(k11 + (lambda_3 * newcp.x), k22 + (lambda_3 * newcp.y), k33 + (lambda_3 * newcp.z));
                    float KneeXl = body.Joints[JointType.ShoulderRight].Position.X; //Impresión en los tres ejes (x,y,z) de la rodilla
                    float KneeYl = body.Joints[JointType.ShoulderRight].Position.Y;
                    float KneeZl = body.Joints[JointType.ShoulderRight].Position.Z;
                    float lambda_4 = (((-newcp.x) * KneeXl + (-newcp.y) * KneeYl + (-newcp.z) * KneeZl) - daux1) / ((newcp.x) * (newcp.x) + (newcp.y) * (newcp.y) + (newcp.z) * (newcp.z));
                    Vector3 Hipproj = new Vector3(KneeXl + (lambda_4 * newcp.x), KneeYl + (lambda_4 * newcp.y), KneeZl + (lambda_4 * newcp.z));
                    Vector3 Hip_A = new Vector3(Kneeproject.x - Hipproj.x, Kneeproject.y - Hipproj.y, Kneeproject.z - Hipproj.z);
                    float AngleFlexl = Vector3.Angle(Vec_Aux1, Hip_A);
                    float AngleFlexlcorrec = 180 - AngleFlexl; // Se le resta a 180 los grados que indique la operación
                    StartCoroutine(Temporizador(AngleFlex1co, AngleFlexlcorrec));
                }
                
            }
        }

    }


    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);

            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.SetVertexCount(2);
            lr.material = BoneMaterial;
            lr.SetWidth(0.0f, 0.0f);

            jointObj.transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }

        return body;


    }

    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;

            if (_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            Transform jointObj = bodyObject.transform.Find(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if (targetJoint.HasValue)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }
        }
    }

    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
            case Kinect.TrackingState.Tracked:
                return Color.green;

            case Kinect.TrackingState.Inferred:
                return Color.red;

            default:
                return Color.black;
        }
    }

    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}


*/

