﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
using UnityEngine.UI;

public class Envioangulo : MonoBehaviour {

    public Text angulo = null;
    public Text angulo2 = null;
    public Text angulo3 = null;
    public Text angulo4 = null;
    public GameObject[] DNI;
    public string nombre;
    public BodySourceUpperBody bS;
    public Text mytext=null;
    public string fecha;
    public float hora;
    public float minutos;
    public string Hora;
    public bool onetime = false;
    public Text Movimiento;
    public Text Simetrianum;
    
    
    //public Text AngleR; // este
    
    // public Text guardar2;

    // Update is called once per frame
    string AngleInsertURL = "localhost/users/AngleInsert.php";//Abre el script AngleInsert.php
 
    void Update()
    {
       // Debug.Log(angulo.text);
        //Debug.Log(angulo2.text);

        //Debug.Log(bS.cont);        
        if (bS.cont == 5 && onetime==false) //&& onetime
        {
            
            //fecha
            System.DateTime dateTime = System.DateTime.Now;
            fecha = dateTime.ToString("dd-MM-yyyy");
            //hora
            hora = System.DateTime.Now.Hour;
            minutos = System.DateTime.Now.Minute;
            Hora = hora + ":" + minutos;
            //DNI

            DNI = GameObject.FindGameObjectsWithTag("DNI");
            nombre = DNI[1].name;
            StartCoroutine(AngleInsert(nombre, angulo.text, angulo2.text, fecha, Hora, Movimiento.text, Simetrianum.text)); //, AngleR.text                                      ,guardar2.text
            onetime = true;
        }

        //mytext.text = nombre;
        //Debug.Log(nombre);
  

    }

    IEnumerator AngleInsert(string nombre, string angulo,string angulo2,string fecha,string Hora,string Movimiento, string Simetrianum)  //, string AngleR                      , string guardar2
    {
      
        WWWForm form = new WWWForm();

        form.AddField("DNIPost", nombre);
        form.AddField("AnguloMaxPost", angulo);
        form.AddField("AnguloMax2Post", angulo2);
        form.AddField("FechaPost", fecha);
        form.AddField("HoraPost", Hora);
       form.AddField("MovimientoPost", Movimiento);
        form.AddField("SimetriaPost", Simetrianum);
        //form.AddField("AngleRPost", AngleR);
        //form.AddField("GuardarPost", guardar2);

        WWW www = new WWW(AngleInsertURL, form);
        yield return www;

      // Debug.Log(www.text);
        //Mensaje.text = www.text;
    } 
}
