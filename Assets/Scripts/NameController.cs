﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//Escena Global Account System,Gameobject con mismo nombre, coje el DNI del login del doctor y no lo destruye al pasar de escena,para despues tener el DNI presente en la otra escena
public class NameController : MonoBehaviour {
    public GameObject UserrName;
    private Text UserName;
    // Use this for initialization

    void Update () {
        UserName = UserrName.GetComponent<Text>();
        gameObject.name = UserName.text;
    }
	
	// No lo destruye en el cambio de escena
	void Awake () {
        DontDestroyOnLoad(gameObject);
    	
	}
}
