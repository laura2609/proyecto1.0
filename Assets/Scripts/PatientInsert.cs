﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// Registro de Pacientes, para pasar todos los datos insertados al servidor (mediante el script IsertUser.php)
/// 
//En la escena Pacients, ir al Register y cada InputField corresponde a cada GameObject de los que vienen a continuación. 
//Se define el texto asociado a cada GameObject con el "private Text".
public class PatientInsert: MonoBehaviour
{
    public GameObject PaatientName;
    private Text PatientName;
    public GameObject PaatientDNI;
    private Text PatientDNI;
    public GameObject DDNI;
    private Text DNI;
    public Text Mensaje = null;
    public InputField PacientsName;
    public InputField DNIi;


    string CreatePatientURL = "localhost/users/InsertPatient.php";//Abre el script InsertPatient.php

    // Se asigna el texto asociado a cada GameObject.
    void Start()
    {
        PatientName = PaatientName.GetComponent<Text>();
        PatientDNI = PaatientDNI.GetComponent<Text>();
        DNI = DDNI.GetComponent<Text>();



    }

   
    public void ButtonClick()
    {
        CreatePatient(PatientName.text, PatientDNI.text,DNI.text);
        PacientsName.Select();
        PacientsName.text = "";
        DNIi.Select();
        DNIi.text = "";


    }
    //Funcion CreatePatient: envía los datos de registro al InsertPacient.php, que luego los envía al servidor.
    public void CreatePatient(string PatientName, string PatientDNI, string DNI)
    {
        WWWForm form = new WWWForm();

        form.AddField("PatientNamePost", PatientName);
        form.AddField("PatientDNIPost", PatientDNI);
        form.AddField("DNIPost", DNI);
       Debug.Log(PatientDNI + PatientName + DNI + "");
        

        WWW www = new WWW(CreatePatientURL, form);
        Mensaje.text = "Usuario Registrado Correctamente";
    }
}
