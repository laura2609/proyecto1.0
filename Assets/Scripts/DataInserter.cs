﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


/// Registro de Doctores, para pasar todos los datos insertados al servidor (mediante el script IsertUser.php)

//En la escena GlobalAccountSystem, ir al Register y cada InputField corresponde a cada GameObject de los que vienen a continuación. 
//Se define el texto asociado a cada GameObject con el "private Text".
public class DataInserter : MonoBehaviour {
    public GameObject fiiirstname;
    private Text FirstName;
    public GameObject LaastName;
    private Text LastName;
  //  public GameObject Aage;
//    private Text Age;
 //   public GameObject Coompany;
 //   private Text Company;
    public GameObject UseerName;
    private Text UserName;
 //   public GameObject Emaiil;
 //   private Text Email;
    public GameObject Paassword;
    private Text Password;
    //Mensaje registrado
    public Text Mensaje = null;


    string CreateUserURL = "localhost/users/InsertUser.php";//Abre el script InsertUser.php

    // Se asigna el texto asociado a cada GameObject.
    void Start () {
    FirstName=   fiiirstname.GetComponent <Text> ();
    LastName = LaastName.GetComponent<Text>();
      //  Age = Aage.GetComponent<Text>();
    //    Company = Coompany.GetComponent<Text>();
        UserName = UseerName.GetComponent<Text>();
     //   Email = Emaiil.GetComponent<Text>();
        Password = Paassword.GetComponent<Text>();
    }

    // Al pulsar el Boton Register, inicia la funcion CreateUser
    
    public void ButtonClick()
    {
        StartCoroutine(CreateUser(FirstName.text, LastName.text,UserName.text, Password.text));
    }
    //Funcion CreateUser: envía los datos de registro al php, que luego los envía al servidor.
    IEnumerator CreateUser(string FirstName, string LastName, string UserName,string Password)
    {
		WWWForm form = new WWWForm();

        form.AddField("FirstNamePost", FirstName);
        form.AddField("LastNamePost", LastName);
        form.AddField("UserNamePost", UserName);
        form.AddField("PasswordPost", Password);

        WWW www = new WWW(CreateUserURL, form);
        yield return www;

        Debug.Log(www.text);
        Mensaje.text = www.text;
    }
}
